package models;

/**
 * The mode to be selected on the M/Chip Mobile application, in order to process the transaction.
 */
public enum McmMode {
	MANAGEMENT,
	CONTACTLESS,
	REMOTE_PAYMENT,
	AUTHENTICATION
}