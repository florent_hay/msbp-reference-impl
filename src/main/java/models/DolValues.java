package models;

import mchipengine.models.transaction.contactless.DolRequestList;
import mchipengine.utils.MchipUtils;

import java.util.Map;

/**
 * Manages the DOL related data that was received from the Reader in a C-APDU.
 * The corresponding DOL must be provided as a DolRequestList object to allow decoding of the received data blob.
 */
public class DolValues {

    /**
     * Object storing the value corresponding to each tag. Each DOL item is mapped as { Tag (String), Value (Byte[]) }
     */
    private final java.util.Map<String, byte[]> mDolValues = new java.util.HashMap<>();

    /**
     * Static factory method to build a data structure representing the DOL Related Data (that is, the values) of the associated Data Object List (DOL).
     *
     * @param dolList   The Data Object List (DOL) that was requested to the reader
     * @param dolValues The DOL Related Data (concatenated Values) as received from the reader (byte[]).
     * @return The object to manage the DOL values
     */
    public static DolValues of(final DolRequestList dolList, final byte[] dolValues) {
        return new DolValues(dolList, dolValues);
    }

    /**
     * Returns the value corresponding to a given tag. Returns null if the tag is not in the present in the DOL.
     *
     * @param tag The TAG for which the value has to be retrieved
     * @return The tag value if found, otherwise an empty tag.
     */
    public byte[] getValueByTag(final String tag) {
        return mDolValues.get(tag);
    }

    /**
     * Private constructor used to build the DolValues attribute based on the DOL and the DOL Related Data.
     *
     * @param dolList The Data Object List (DOL) requested to the reader
     * @param values  The DOL Related Data (concatenated values) provided by the reader.
     */
    private DolValues(final DolRequestList dolList, final byte[] values) {
        if (dolList != null && values != null) {
            int currentPosition = 0;
            for (DolRequestList.DolItem dolItem : dolList.getRequestList()) {
                if (currentPosition + dolItem.getLength() > values.length) {
                    break;
                }
                final byte[] value = new byte[dolItem.getLength()];
                System.arraycopy(values, currentPosition, value, 0, dolItem.getLength());
                currentPosition += dolItem.getLength();
                mDolValues.put(dolItem.getTag(), value);
            }
        }
    }

    /**
     * This function is mainly for debug purposes. It returns a string presenting a list of [Tag || Value].
     */
    @Override
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();
        for (Map.Entry<String, byte[]> entry : mDolValues.entrySet()) {
            stringBuilder.append("[").append(entry.getKey()).append(" | ").append(MchipUtils.byteArrayToHexString(entry.getValue())).append("]");
        }
        return stringBuilder.toString();
    }

}