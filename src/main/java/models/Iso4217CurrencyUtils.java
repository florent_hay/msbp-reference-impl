package models;

import mchipengine.utils.MchipUtils;

import java.util.Currency;
import java.util.Map;
import java.util.Set;

/**
 * Utility class to get the ISO 4217 Numeric Codes.
 * Although Java supports it, the ability to parse through numeric codes is not available on Android.
 * Source: ISO 4217:2015 (http://www.iso.org/iso/home/standards/currency_codes.htm)
 */
public class Iso4217CurrencyUtils {

    private static Map<String, String> sCodes = new java.util.HashMap<>();

    static {
        Set<Currency> availableCurrencies = Currency.getAvailableCurrencies();
        for (Currency currency : availableCurrencies) {
            sCodes.put(String.valueOf(currency.getNumericCode()), currency.getCurrencyCode());
        }
    }

    /**
     * Get the currency code from a numeric value
     *
     * @param numericCode The Numeric Code as String
     * @return The Currency Code
     */
    public static String getCodeFromNumericValue(final String numericCode) {
        return sCodes.get(numericCode);
    }

    /**
     * Utility function to calculate the fraction digits for a given currency. If the currency isnull, fraction digits will be returned as 0
     *
     * @return The fraction digit for this currency. 0, if the currency is null.
     */
    private static int getFractionDigits(final Currency currency) {
        final int fractionDigits;
        if (currency != null) {
            fractionDigits = currency.getDefaultFractionDigits();
        } else {
            fractionDigits = 0;
        }
        return fractionDigits;
    }

    /**
     * Utility function to get the Currency from the numeric code (e.g. EURO has code 978).
     *
     * @param code The ISO 4217 Numeric code as byte[]
     * @return The Currency associated with that numeric code. Null if the currency could not be found
     */
    public static Currency getCurrencyByCode(final byte[] code) {
        try {
            // We need to take exact the characters 1,2,3 excluding 0 and any other trailer.
            // It seems in the db this is stored as 3 bytes value, so we need to remove trailer zeroes.
            final String value = MchipUtils.byteArrayToHexString(code);
            String currencyNumericValue = Iso4217CurrencyUtils.getCodeFromNumericValue(value);
            if (currencyNumericValue == null) {
                return null;
            }
            return Currency.getInstance(currencyNumericValue);
        } catch (final IllegalArgumentException e) {
            // We could not find the currency
            return null;
        }
    }

    /**
     * Utility function to calculate the exact amount for a given currency code.For example, 5199 USD is converted into 55,99 USD as USD has 2 fraction digits
     *
     * @param amount   The transaction amount as BCD data (as received in the C-APDU)
     * @param currency The currency for the amount
     * @return The amount as double
     */
    public static double convertBcdAmountToDouble(final byte[] amount, final Currency currency) {
        final int fractionDigits = getFractionDigits(currency);
        final double amountAsDouble = (double) Long.valueOf(MchipUtils.byteArrayToHexString(amount), 10);
        return amountAsDouble / (fractionDigits > 0 ? Math.pow(10, fractionDigits) : 1);
    }

    /**
     * Utility function to calculate the exact amount for a given currency code.Please note that this function assumes binary data as Input
     *
     * @param amount   The transaction amount as binary data. This is used for Amount Other in the transaction data
     * @param currency The currency for the amount
     * @return The amount as do uble
     */
    public static double convertBinaryAmountToDouble(final byte[] amount, final Currency currency) {
        final int fractionDigits = getFractionDigits(currency);
        long amountAsLong = 0;
        for (int i = 0; i < amount.length; i++) {
            // AND with 0x00FF ensures that the positive sign is preserved!
            amountAsLong += ((amount[amount.length - 1 - i] & 0x00FF) << (i * 8));
        }
        return (double) amountAsLong / (fractionDigits > 0 ? Math.pow(10, fractionDigits) : 1);
    }

    /**
     * Get the Numeric Currency Code from the Currency Code (e.g. "EUR" -> 978)
     *
     * @param currencyCode The Currency Code as String (e.g. "USD", "EUR", etc)
     */
    public int getNumericCodeFromCode(String currencyCode) {
        for (Map.Entry<String, String> item : sCodes.entrySet()) {
            final String value = item.getValue();
            if (value.equalsIgnoreCase(currencyCode)) {
                return Integer.valueOf(item.getKey());
            }
        }
        // No value could be found
        return 0;
    }

}