package models;

/**
 * This data structure is returned to the wallet, when a DSRP transaction is initiated, using the method processDsrpTransaction()
 */
public class DsrpOutputData {

    /**
     * A string containing the PAN of the card with 'F' padding removed (if any)
     * Example: If getPan() returns a byte[] with the value: 67031234567890123F then the string value that will be present in this field will be 67031234567890123
     */
    private final String mPan;
    /**
     * An integer value specifying the PAN Sequence Number (PSN) of the card used.
     */
    private final int mPanSequenceNumber;
    /**
     * No Pad with one Hex 'F' to ensure whole bytes.
     */
    private String mPar;
    /**
     * The expiration date of the card.
     */
    final Date mExpirationDate;

    /**
     * Contains the data elements of track 2 according to ISO/IEC 7813, excluding start sentinel, end sentinel, and Longitudinal Redundancy Check (LRC), as follows:
     * Primary Account Number
     * Field Separator (Hex 'D')
     * Expiration Date (YYMM)
     * Service Code
     * Discretionary Data (defined by individual payment systems)
     * No Pad with one Hex 'F' to ensure whole bytes
     */
    private final String mTrack2EquivalentData;
    private final CryptogramType mCryptogramType;
    /**
     * A byte array containing a formatted response, either UCAF or a set of TLV data for the merchant to populate a DE55 field
     */
    private final byte[] mCryptogramData;

    /**
     * Default constructor:
     * mPan = pan;
     * mPanSequenceNumber = panSequenceNumber;
     * mPar = par;
     * mExpirationDate = expirationDate;
     * mTrack2EquivalentData = track2EquivalentData;
     * mCryptogramType = cryptogramType;
     * mCryptogramData = cryptogramData;
     *
     * @param pan                  A string containing the PAN of the card used
     * @param panSequenceNumber    An integer value specifying the PAN Sequence Number (PSN) of the card used.
     * @param par
     * @param expirationDate       A parameter of type date, specifying the expiration date of the card used.
     * @param track2EquivalentData Contains the data elements of track 2 according to ISO/IEC 7813, excluding start sentinel, end sentinel, and Longitudinal Redundancy Check (LRC), as follows: Primary Account Number
     *                             Field Separator (Hex 'D')
     *                             Expiration Date (YYMM)
     *                             Service Code
     *                             Discretionary Data (defined by individual payment systems)
     *                             Pad with one Hex 'F' if needed to ensure whole bytes
     * @param cryptogramData       A byte array containing a formatted response, either UCAF or a set of TLV data for the merchant to populate DE-55 data
     * @param cryptogramDataType   An enum of type CryptogramDataType. It represents the type of chip cryptogram that the Merchant or the Payment Gateway has requested.
     */
    public DsrpOutputData(final String pan, final int panSequenceNumber, String par, final Date expirationDate,
                          final String track2EquivalentData, final byte[] cryptogramData, CryptogramType cryptogramDataType) {
        this.mPan = pan;
        this.mPanSequenceNumber = panSequenceNumber;
        this.mPar = par;
        this.mExpirationDate = expirationDate;
        this.mTrack2EquivalentData = track2EquivalentData;
        this.mCryptogramData = cryptogramData;
        this.mCryptogramType = cryptogramDataType;
    }

    /**
     * Returns mCryptogramData; (A byte array containing a formatted response, either UCAF or a set of TLV data for the merchant to populate DE-55 data)
     */
    public byte[] getCryptogramData() {
        return this.mCryptogramData;
    }

    /**
     * Return mCryptogramType; (An enum of type CryptogramType representing the type of cryptogram data field that the Merchant or the Payment Gateway has requested)
     */
    public CryptogramType getCryptogramType() {
        return this.mCryptogramType;
    }

    /**
     * Returns mExpirationDate; (the expiration date of the card used)
     */
    public Date getExpirationDate() {
        return this.mExpirationDate;
    }

    /**
     * Returns mPan; (the PAN of the card used)
     */
    public String getPan() {
        return this.mPan;
    }

    /**
     * Returns mPanSequenceNumber; (the PAN Sequence Number (PSN) of the card used)
     */
    public int getPanSequenceNumber() {
        return this.mPanSequenceNumber;
    }

    /**
     * Returns mPar; (the PAR of the card used)
     */
    public String getPar() {
        return this.mPar;
    }

    /**
     * Return the Track2EquivalentData of the card used.
     */
    public String getTrack2EquivalentData() {
        return this.mTrack2EquivalentData;
    }

}