package models;

/**
 * Indicate what type of cryptogram data format must be returned (UCAF Version 0, UCAF Version 0+, UCAF Version 3, DE55)
 */
public enum CryptogramType {
	/**
	 * Merchant supports and requests a Universal Cardholder Authentication Field (UCAF) Version 0 in Data Element 48 Subelement 43 for the DSRP Authorization Request Message.
	 */
	UCAF_V0,
	/**
	 * Merchant supports and requests a Universal Cardholder Authentication Field (UCAF) Version 0+ in Data Element 48 Subelement 43 for the DSRP Authorization Request Message.
	 */
	UCAF_V0_PLUS,
	/**
	 * Merchant supports and requests a Universal Cardholder Authentication Field (UCAF) Version 3 in Data Element 48 Subelement 43 for the DSRP Authorization Request Message.
	 */
	UCAF_V3,
	/**
	 * Merchant supports and requests chip data in Data Element 55 for the DSRP Authorization Request Message.
	 */
	DE55
}