package models;

import java.util.Calendar;

/**
 * Utility class to manage Date
 */
public class Date implements Comparable<Date> {

    /**
     * Holds the year of the date as an integer.
     */
    private int mYear;
    /**
     * Holds the day of the date as an integer.
     */
    private int mDay;
    /**
     * Holds the month of the date as an integer.
     */
    private int mMonth;

    /**
     * Create a Date object based on year, month and day:
     * mYear = year;
     * mMonth = month;
     * mDay = day;
     *
     * @param year
     * @param month
     * @param day
     */
    public Date(final int year, final int month, final int day) {
        this.mYear = year;
        this.mMonth = month;
        this.mDay = day;
    }

    /**
     * Create a Date object based on the actual system time:
     * <p>
     * mYear = SystemDate(year);
     * mMonth = SystemDate(month);
     * mDay = SystemDate(day);
     */
    public Date() {
        Calendar cal = Calendar.getInstance();

        this.mYear = cal.get(Calendar.YEAR);
        this.mMonth = cal.get(Calendar.MONTH);
        this.mDay = cal.get(Calendar.DAY_OF_MONTH);
    }

    /**
     * Return mYear.
     */
    public int getYear() {
        return mYear;
    }

    /**
     * Set mYear to the new value pYear  .
     *
     * @param year
     */
    public void setYear(int year) {
        this.mYear = year;
    }

    /**
     * Return mDay.
     */
    public int getDay() {
        return mDay;
    }

    /**
     * Set mDay to the new value day.
     *
     * @param day
     */
    public void setDay(int day) {
        this.mDay = day;
    }

    /**
     * Return mMonth.
     */
    public int getMonth() {
        return this.mMonth;
    }

    /**
     * Set mMonth to the new value month.
     *
     * @param month
     */
    public void setMonth(int month) {
        this.mMonth = month;
    }

    /**
     * Check the validity of the date.
     * A month value greater than 12, or a day not included in the month will return FALSE
     */
    public boolean isValid() {
        if (mMonth > 12) {
            return false;
        } else if (mMonth == 1 && mDay > 31) {
            return false;
        } else if (mMonth == 2 && mDay > 29) {
            return false;
        } else if (mMonth == 3 && mDay > 31) {
            return false;
        } else if (mMonth == 4 && mDay > 30) {
            return false;
        } else if (mMonth == 5 && mDay > 31) {
            return false;
        } else if (mMonth == 6 && mDay > 30) {
            return false;
        } else if (mMonth == 7 && mDay > 31) {
            return false;
        } else if (mMonth == 8 && mDay > 31) {
            return false;
        } else if (mMonth == 9 && mDay > 30) {
            return false;
        } else if (mMonth == 10 && mDay > 31) {
            return false;
        } else if (mMonth == 11 && mDay > 30) {
            return false;
        } else return mMonth != 12 || mDay <= 31;
    }

    /**
     * Get Date value in byte array:
     * The date is returned as a byte array in the following format: YYMMDD
     */
    public byte[] getDateAsByteArray() {
        return String.format("%02d%02d%02d", mYear % 100, mMonth, mDay).getBytes();
    }

    /**
     * @param o
     * @return -1 if this date is prior to the provided argument
     * 0 if this date has the same date as the provided argument
     * 1 if this date is after the provided argument
     */
    @Override
    public int compareTo(Date o) {
        if (this.mYear == o.getYear()) {
            // Same year...
            if (this.mMonth == o.getMonth()) {
                // Same month also...
                if (this.mDay == o.getDay()) {
                    // Same day...
                    return 0;
                } else {
                    return this.mDay > o.getDay() ? 1 : -1;
                }
            } else {
                return this.mMonth > o.getMonth() ? 1 : -1;
            }
        } else {
            return this.mYear > o.getYear() ? 1 : -1;
        }
    }
}