package models;

/**
 * MchipCryptoOutcome is an object returned by the method generateMchipCryptoOutcome().  It combines all crypto information (application cryptogram, CVR, IAD, ATC, CID) computed during the transaction and makes them available for constructing the transaction record.
 */
public class MchipCryptoOutcome {

    /**
     * Holds the Application Transaction Counter of the transaction.
     */
    private byte[] mAtc;
    /**
     * Holds the CID of the transaction.
     */
    private byte mCid;
    /**
     * Holds the CVR of the transaction.
     */
    private byte[] mCvr;

    private byte[] mApplicationCryptogram;

    /**
     * Holds the Issuer Application Data of the transaction.
     */
    private byte[] mIssuerApplicationData;

    /**
     * mAtc = atc;
     * mCid = cid;
     * mCvr = cvr;
     * mMdCryptogram = mdCryptogram;
     * mUmdCryptogram = umdCryptogram;
     * mIssuerApplicationData = issuerApplicationData;
     *
     * @param atc
     * @param cid
     * @param cvr
     * @param ac
     * @param issuerApplicationData
     */
    public MchipCryptoOutcome(byte[] atc, byte cid, byte[] cvr, byte[] ac, byte[] issuerApplicationData) {
        this.mAtc = atc;
        this.mCid = cid;
        this.mCvr = cvr;
        this.mApplicationCryptogram = ac;
        this.mIssuerApplicationData = issuerApplicationData;
    }

    /**
     * Returns mApplicationCryptogram;
     */
    public byte[] getApplicationCryptogram() {
        return this.mApplicationCryptogram;
    }

    /**
     * Returns mIssuerApplicationData;
     */
    public byte[] getIssuerApplicationData() {
        return this.mIssuerApplicationData;
    }

    /**
     * Returns mCvr;
     */
    public byte[] getCvr() {
        return this.mCvr;
    }

    /**
     * Returns mCid;
     */
    public byte getCid() {
        return this.mCid;
    }

    /**
     * Returns mAtc;
     */
    public byte[] getAtc() {
        return mAtc;
    }

}