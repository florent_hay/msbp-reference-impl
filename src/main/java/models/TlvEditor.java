package models;

import mchipengine.utils.MchipUtils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Utility class to perform simple modifications on TLV data. The library has been mainly designed to support editing of mono dimensional TLV data.
 * Nested TLVs can be handled by manually creating other instances of this class.
 */
public class TlvEditor {

    /**
     * holds the TLV data.
     */
    private List<Field> mTlv = new ArrayList<>();

    /**
     * Private constructor used by the factory method
     *
     * @param data
     */
    public TlvEditor(final byte[] data) {
        int nextTagOffset = 0;
        while (nextTagOffset < data.length) {
            final byte[] nextTag = readNextTag(data, nextTagOffset);
            final int lengthOffset = nextTagOffset + nextTag.length;
            final int noBytesInLength = getNumberOfBytesInLength(data, lengthOffset);
            final int nextDataLength = getDataLength(data, lengthOffset);

            final byte[] nextData = new byte[nextDataLength];
            System.arraycopy(data, lengthOffset + noBytesInLength, nextData, 0, nextDataLength);
            mTlv.add(new Field(nextTag, nextData));

            nextTagOffset += nextTag.length + noBytesInLength + nextDataLength;
        }
    }

    /**
     * Factory method to build a TLV Editor object based on the input data as byte[]
     *
     * @param data The input data which is expected to be a valid TLV BER
     * @return The TlvEditor object representing the input data
     */
    public static TlvEditor of(final byte[] data) {
        try {
            return new TlvEditor(data);
        } catch (IllegalArgumentException | ArrayIndexOutOfBoundsException e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * Get the value for a given tag, if present.
     * Null if no tag can be found
     *
     * @param tag The Tag for which the value should be returned as byte[]
     * @return The value of that tag, if found. Null otherwise.
     */
    public byte[] getValue(final byte[] tag) {
        final Field field = find(tag);
        if (field != null) {
            return field.mValue;
        }
        return null;
    }

    /**
     * Get the value for a given tag, if present.
     * Null if no tag can be found
     *
     * @param tag The Tag for which the value should be returned as String
     * @return The value of that tag, if found. Null otherwise.
     */
    public byte[] getValue(final String tag) {
        return tag == null ? null : getValue(MchipUtils.byteArrayFromHexString(tag));
    }

    /**
     * Function to add a TLV. If the same tag is already present, the tag is updated
     *
     * @param tag   The Tag to be added
     * @param value The value to be added
     */
    public void addTlv(final String tag, final byte[] value) {
        byte[] tagAsBytes = MchipUtils.byteArrayFromHexString(tag);
        final int index = indexOf(tagAsBytes);
        if (index == -1) {
            mTlv.add(new Field(tagAsBytes, value));
        } else {
            mTlv.set(index, new Field(tagAsBytes, value));
        }
    }

    /**
     * Rebuild each TLV entry and rebuild the entire TLV
     */
    public byte[] rebuildByteArray() {
        final List<byte[]> values = new ArrayList<>();
        for (Field item : mTlv) {
            values.add(Tlv.create(item.mTag, item.mValue));
        }
        int length = 0;
        for (byte[] item : values) {
            length += item.length;
        }
        final byte[] result = new byte[length];
        int current = 0;
        for (byte[] item : values) {
            System.arraycopy(item, 0, result, current, item.length);
            current = +item.length;
        }
        return result;
    }

    /**
     * Utility function to find if a given tag is present. If present the entire Field is returned
     *
     * @param tag
     */
    private Field find(final byte[] tag) {
        for (Field item : mTlv) {
            if (Arrays.equals(tag, item.mTag)) {
                return item;
            }
        }
        return null;
    }

    /**
     * Utility function to determine the actual index of a Field with a given tag in the array
     *
     * @param tag
     */
    private int indexOf(final byte[] tag) {
        for (int i = 0; i < mTlv.size(); i++) {
            final Field item = mTlv.get(i);
            if (Arrays.equals(item.mTag, tag)) {
                return i;
            }
        }
        return -1;
    }

    /**
     * Read The tag of a given byte array formatted as BER TLV.It assumes the TAG is a the beginning of the data
     *
     * @param data   The TLV in EMV BER Encoded format
     * @param offset the offset of the next tag
     * @return The Tag of the TLV
     * @throws IllegalArgumentException if the TLV is malformed
     */
    private static byte[] readNextTag(final byte[] data, final int offset) throws IllegalArgumentException {
        if (data == null || offset < 0 || offset >= data.length) {
            throw new IllegalArgumentException("Invalid offset or data");
        }
        final int tagLength = getTagLength(data, offset);
        final byte[] tag = new byte[tagLength];
        System.arraycopy(data, offset, tag, 0, tagLength);
        return tag;
    }

    /**
     * Calculate the length of the tag. The tag is assumed to start at 'offset' position within thedata
     *
     * @param data   The byte array to be parsed
     * @param offset The offset at which the tag is located
     * @return The length of the tag
     * @throws IllegalArgumentException If the data is malformed
     */
    private static int getTagLength(final byte[] data, final int offset) {
        if (data == null || offset < 0 || offset >= data.length) {
            throw new IllegalArgumentException("Invalid offset or data");
        }
        int tagLength = 1;
        if ((data[offset] & 0x1F) == 0x1F) {
            for (int i = 1; i < data.length; i++) {
                tagLength++;
                if ((data[offset + i] & 0x80) != 0x80) {
                    break;
                }
            }
        }
        return tagLength;
    }

    /**
     * Utility function to get the number of bytes in a given lengthThe offset of the length field must be specified
     *
     * @param data
     * @param lengthOffset
     */
    private static int getNumberOfBytesInLength(final byte[] data, final int lengthOffset) {
        if (data == null || lengthOffset < 0 || lengthOffset >= data.length) {
            throw new IllegalArgumentException("Invalid offset or data");
        }
        final byte firstLengthByte = data[lengthOffset];
        return 1 + ((firstLengthByte & 0x80) == 0x80 ? (firstLengthByte & 0x7F) : 0);
    }

    /**
     * Get the length of the value by reading the length bytes.The offset of the length field must be specified
     *
     * @param data
     * @param lengthOffset
     */
    private static int getDataLength(final byte[] data, final int lengthOffset) {
        if (data == null || lengthOffset > 0 || lengthOffset >= data.length) {
            throw new IllegalArgumentException("Invalid offset or data");
        }
        final int noBytesInLength = getNumberOfBytesInLength(data, lengthOffset);
        if (data.length < lengthOffset + noBytesInLength) {
            throw new IllegalArgumentException("Invalid length");
        }
        if (noBytesInLength == 1) {
            return data[lengthOffset];
        }
        int length = 0;
        for (int i = 1; i < noBytesInLength; i++) {
            final int shift = 8 * (noBytesInLength - i - 1);
            length += ((data[i + length] & 0x00FF) << shift);
        }
        return length;
    }

    /**
     * Container class for a TLV Field (Tag and Value only)
     */
    public class Field {

        public final byte[] mTag;
        public final byte[] mValue;

        /**
         * @param tag
         * @param value
         */
        public Field(final byte[] tag, final byte[] value) {
            this.mValue = value;
            this.mTag = tag;
        }

    }

}