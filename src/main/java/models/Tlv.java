package models;

/**
 * Static helper functions to handle Tlv data.
 */
public enum Tlv {
    INSTANCE;

    /**
     * Creates a TLV byte array from a tag and a value, as per EMV Book 3 Annex B.
     * - The tag is provided in input (as a byte[])
     * - The length, corresponding to the length of the value, is encoded on 1 or 2 bytes as per EMV Book 3 Annex B3
     * - The value is provided in input (as a byte[])
     *
     * @param pTag   the Tlv tag
     * @param pValue the Tlv value
     * @return the Tlv as byte[]
     */
    public static byte[] create(byte[] pTag, byte[] pValue) {
        byte[] length = lengthBytes(pValue);
        byte[] result = new byte[pTag.length + length.length + pValue.length];
        System.arraycopy(pTag, 0, result, 0, pTag.length);
        System.arraycopy(length, 0, result, pTag.length, length.length);
        System.arraycopy(pValue, 0, result, pTag.length + length.length, pValue.length);
        return result;
    }

    /**
     * Calculates the length field of a TLV data element according to BER-TLV rules (EMV Book 3 Annex B3), based on the value of the TLV object. The length field is encoded on 1 or 2 bytes as per EMV Book 3 Annex B3.
     *
     * @param value the value for which the length is returned
     * @return the length of the value.
     */
    private static byte[] lengthBytes(byte[] value) {
        int length = value.length;
        if (value.length <= 0x7F) {
            return new byte[]{(byte) length};
        }
        if (value.length <= 0xFF) {
            return new byte[]{(byte) 0x81, (byte) (length & 0xFF)};
        }
        if (value.length <= 0xFFFF) {
            byte[] result = new byte[3];
            result[0] = (byte) 0x82;
            result[1] = (byte) ((length & 0x0000FF00) >> 8);
            result[2] = (byte) ((length & 0x000000FF));
            return result;
        }
        if (value.length <= 0xFFFFFF) {
            byte[] result = new byte[4];
            result[0] = (byte) 0x83;
            result[1] = (byte) ((length & 0x00FF0000) >> 16);
            result[2] = (byte) ((length & 0x0000FF00) >> 8);
            result[3] = (byte) ((length & 0x000000FF));
            return result;
        }
        byte[] result = new byte[5];
        result[0] = (byte) 0x84;
        result[1] = (byte) ((length & 0xFF000000) >> 24);
        result[2] = (byte) ((length & 0x00FF0000) >> 16);
        result[3] = (byte) ((length & 0x0000FF00) >> 8);
        result[4] = (byte) ((length & 0x000000FF));
        return result;
    }

}