package models;

import mchipengine.utils.MchipUtils;

/**
 * This is the class used to store all data elements required to compute an M/Chip cryptogram
 * <p>
 * Elements are prepared, based on type of cryptogram requested by the wallet.
 */
public class CryptogramInput {

    private byte[] mAmountAuthorized;
    private byte[] mAmountOther = new byte[]{0x00, 0x00, 0x00, 0x00, 0x00, 0x00};
    private byte[] mTerminalCountryCode;
    private byte[] mTerminalVerificationResults = new byte[]{0x00, 0x00, 0x00, 0x00, 0x00};
    private byte[] mTransactionCurrencyCode;
    private byte[] mTransactionDate;
    private byte mTransactionType;
    private byte[] mUnpredictableNumber = null;

    /**
     * Returns an object of type CryptogramInput for DSRP DE55
     *
     * @param dsrpInputData The input data provided by the merchant.
     * @param aip
     */
    public static CryptogramInput forDsrp(DsrpInputData dsrpInputData, byte[] aip) {
        // CI55.1
        byte[] amountAuthorized = MchipUtils.longToBcd(dsrpInputData.getAmount(), 6);

        // CI55.2
        byte[] currencyCode = MchipUtils.longToBcd(dsrpInputData.getCurrencyCode(), 2);

        // CI55.3
        byte[] unpredictableNumber = new byte[4];
        MchipUtils.writeInt(unpredictableNumber, 0, dsrpInputData.getUnpredictableNumber());

        // CI55.4
        byte transactionType = dsrpInputData.getTransactionType();

        // CI55.5
        byte[] transactionDate = dsrpInputData.getTransactionDate().getDateAsByteArray();

        // CI55.6
        byte[] countryCode = MchipUtils.longToBcd(dsrpInputData.getCountryCode(), 2);

        byte[] tvr = new byte[]{0x00, 0x00, 0x00, 0x00, 0x00};

        return new CryptogramInput(
                amountAuthorized,
                countryCode,
                tvr,
                currencyCode,
                transactionDate,
                transactionType,
                unpredictableNumber);
    }

    /**
     * Returns mAmountAuthorized;
     */
    public byte[] getAmount() {
        return this.mAmountAuthorized;
    }

    /**
     * Set mAmountAuthorized to the value passed as a parameter.
     *
     * @param amount
     */
    public void setAmountAuthorized(byte[] amount) {
        this.mAmountAuthorized = amount;
    }

    /**
     * Returns mTerminalCountryCode;
     */
    public byte[] getTerminalCountryCode() {
        return this.mTerminalCountryCode;
    }

    /**
     * Set mTerminalCountryCode to the value passed as a parameter.
     *
     * @param countryCode
     */
    public void setTerminalCountryCode(byte[] countryCode) {
        this.mTerminalCountryCode = countryCode;
    }

    /**
     * Returns mTransactionCurrencyCode;
     */
    public byte[] getTransactionCurrencyCode() {
        return this.mTransactionCurrencyCode;
    }

    /**
     * Set mTransactionCurrencyCode to the value passed as a parameter.
     *
     * @param currencyCode
     */
    public void setTransactionCurrencyCode(byte[] currencyCode) {
        this.mTransactionCurrencyCode = currencyCode;
    }

    /**
     * Returns mTransactionDate;
     */
    public byte[] getTransactionDate() {
        return this.mTransactionDate;
    }

    /**
     * Set mTransactionDate to the value passed as a parameter.
     *
     * @param date
     */
    public void setTransactionDate(byte[] date) {
        this.mTransactionDate = date;
    }

    /**
     * Returns mTransactionType;
     */
    public byte getTransactionType() {
        return this.mTransactionType;
    }

    /**
     * Set mTransactionType to the value passed as a parameter.
     *
     * @param transactionType
     */
    public void setTransactionType(byte transactionType) {
        this.mTransactionType = transactionType;
    }

    /**
     * Returns mUnpredictableNumber
     */
    public byte[] getUnpredictableNumber() {
        return this.mUnpredictableNumber;
    }

    /**
     * return mTerminalVerificationResults
     */
    public byte[] getTvr() {
        return this.mTerminalVerificationResults;
    }

    /**
     * Private constructor.
     *
     * @param amountAuthorized
     * @param terminalCountryCode
     * @param terminalVerificationResults
     * @param transactionCurrencyCode
     * @param transactionDate
     * @param transactionType
     * @param unpredictableNumber
     */
    private CryptogramInput(byte[] amountAuthorized, byte[] terminalCountryCode, byte[] terminalVerificationResults,
                            byte[] transactionCurrencyCode, byte[] transactionDate, byte transactionType,
                            byte[] unpredictableNumber) {
        this.mAmountAuthorized = amountAuthorized;
        this.mTerminalCountryCode = terminalCountryCode;
        this.mTerminalVerificationResults = terminalVerificationResults;
        this.mTransactionCurrencyCode = transactionCurrencyCode;
        this.mTransactionDate = transactionDate;
        this.mTransactionType = transactionType;
        this.mUnpredictableNumber = unpredictableNumber;
    }

}