package models;

/**
 * The DsrpInputDataBuilder provides a builder object that offers a service to easily create a structure of type DsrpInputData that is used in the processDsrpTransaction() method.
 */
public class DsrpInputDataBuilder {

    protected long mAmount = 0;
    protected char mCurrencyCode = 0;
    protected byte mTransactionType = 0x00;
    protected long mUnpredictableNumber = 0;
    protected Date mTransactionDate = new Date();
    protected char mCountryCode = 0;

    /**
     * Indicates whether UCAF or DE55 cryptogram data format must be returned.
     */
    protected CryptogramType mCryptogramType;
    private String mMerchantId = "";

    /**
     * Builder constructor.
     */
    public DsrpInputDataBuilder() {
    }

    /**
     * mAmount = amount
     *
     * @param amount The amount of the transaction
     */
    public DsrpInputDataBuilder withAmount(long amount) {
        this.mAmount = amount;
        return this;
    }

    /**
     * mUnpredictableNumber = unpredictableNumber
     *
     * @param unpredictableNumber A random number provided by the merchant
     */
    public DsrpInputDataBuilder withUnpredictableNumber(long unpredictableNumber) {
        this.mUnpredictableNumber = unpredictableNumber;
        return this;
    }

    /**
     * mCurrencyCode = currencyCode
     *
     * @param currencyCode The currency code of the transaction
     */
    public DsrpInputDataBuilder withCurrencyCode(char currencyCode) {
        this.mCurrencyCode = currencyCode;
        return this;
    }

    /**
     * mTransactionType = transactionType
     *
     * @param transactionType
     */
    public DsrpInputDataBuilder withTransactionType(byte transactionType) {
        this.mTransactionType = transactionType;
        return this;
    }

    /**
     * mTransactionDate = transactionDate
     *
     * @param transactionDate The date of the transaction
     */
    public DsrpInputDataBuilder withTransactionDate(Date transactionDate) {
        this.mTransactionDate = transactionDate;
        return this;
    }

    /**
     * mCountryCode = countryCode
     *
     * @param countryCode The country code of the merchant
     */
    public DsrpInputDataBuilder withCountryCode(char countryCode) {
        this.mCountryCode = countryCode;
        return this;
    }

    /**
     * mCryptogramType = cryptogramType
     *
     * @param cryptogramType the type of cryptogram data format to be returned (UCAF_V0, UCAF_V0_PLUS, UCAF_V3 or DE55).
     */
    public DsrpInputDataBuilder withCryptogramType(CryptogramType cryptogramType) {
        this.mCryptogramType = cryptogramType;
        return this;
    }

    /**
     * mMerchantId = merchantId
     *
     * @param merchantId The merchant ID to be used for the cryptogram computation (only applicable to UCAF_V3)
     */
    public DsrpInputDataBuilder withMerchantId(String merchantId) {
        this.mMerchantId = merchantId;
        return this;
    }

    /**
     * DsrpInputData DsrpInputData = new DsrpInputData(mAmount, mCurrencyCode, mTransactionType, mUnpredictableNumber, mTransactionDate, mCountryCode, mCryptogramType).
     * <p>
     * if        dsrpInputData.isDsrpInputDataValid == False {
     * Throw IllegalStateException
     * }
     * <p>
     * return dsrpInputData
     */
    public DsrpInputData build() throws IllegalStateException {
        DsrpInputData dsrpInputData = new DsrpInputData(
                mAmount,
                mCurrencyCode,
                mTransactionType,
                mUnpredictableNumber,
                mTransactionDate,
                mCountryCode,
                mCryptogramType,
                mMerchantId
        );

        if (dsrpInputData.isDsrpInputDataValid()) {
            return dsrpInputData;
        } else {
            throw new IllegalStateException();
        }
    }

}