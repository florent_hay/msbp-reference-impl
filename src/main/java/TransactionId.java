import mchipengine.utils.CryptoUtils;
import mchipengine.utils.MchipUtils;

import java.math.BigInteger;

/**
 * When the transaction is performed by a payment engine, it calculates the Transaction Identifier from the transaction data.
 * When the same transaction data is received by MDES, it also performs the same calculation and stores the Transaction Identifier with the transaction details record. When the Wallet is notified of the transaction (using the Notify Transaction Details API) and calls the MDES Get Transactions API to fetch transaction details, the Transaction Identifier can be used by the Wallet to match the original transaction event (for example: contactless tap, or a DSRP payment) to the transaction details record provided by the Transaction Details Service.
 * There are three variations of the Transaction Identifier   algorithm :
 * �       Algorithm for M/Chip transactions (contactless,  and  DSRP with full EMV (DE55) data)
 * �       Algorithm for Magnetic Stripe transactions (contactless)
 * �       Algorithm for DSRP transactions with UCAF data
 */
public class TransactionId {

    /**
     * Algorithm for DSRP transactions with UCAF data Applicability
     * <p>
     * This algorithm applies to:
     * * DSRP transactions with UCAF data
     * <p>
     * Definition
     * SHA-256 (pan || ucaf)
     * Where:
     * || represents concatenation of byte arrays
     * pan is encoded per EMV specifications tag '5A' as a compressed numeric two numeric digits (having values in the range Hex '0' to '9') per byte, which are left justified and padded with trailing hexadecimal 'F�s. To avoid any possible ambiguity, any excessive trailing 'FF' bytes are trimmed from the end so that the PAN contains a maximum of one 'F' nibble at the end when the PAN has an odd length.
     * ucaf is encoded in binary format (before applying any base-64 or other encoding when being sent on to the Mastercard network).
     * <p>
     * Example
     * Given:
     * pan (compressed numeric value, giving an 8-byte hex value): 5413339000001513
     * ucaf (binary value, giving a 19-byte hex value): 00745B8C182C9F60DE0005ECCB6EFF02005514
     * Then:
     * Concatenate the two values together to form a 27-byte hex value of 541333900000151300745B8C182C9F60DE0005ECCB6EFF02005514
     * SHA-256 hash the 27-byte value, giving a Transaction Identifier value of f2a491c260e132989522d3748dfcce9361d74e821ca40dda43b74d9ca470546a
     *
     * @param pan
     * @param ucaf
     */
    public static byte[] buildForUcaf(byte[] pan, byte[] ucaf) {
        MchipUtils.ByteArrayBuilder builder = new MchipUtils.ByteArrayBuilder();

        builder.add(pan);
        builder.add(ucaf);

        byte[] transactionId = CryptoUtils.sha256(builder.build());

        return transactionId;
    }

    /**
     * Algorithm for DSRP transactions with DE55 data Applicability
     * <p>
     * This algorithm applies to:
     * * DSRP transactions with DE55 data
     * <p>
     * Definition
     * SHA-256 (pan || atc || applicationCryptogram)
     * <p>
     * Where:
     * || represents concatenation of byte arrays
     * pan is encoded per EMV specifications tag '5A' as a compressed numeric two numeric digits (having values in the range Hex '0' to '9') per byte, which are left justified and padded with trailing hexadecimal 'F�s. To avoid any possible ambiguity, any excessive trailing 'FF' bytes are trimmed from the end so that the PAN contains a maximum of one 'F' nibble at the end when the PAN has an odd length.
     * atc is the Application Transaction Counter, exactly two bytes and encoded per EMV specifications tag '9F36'.
     * Application Cryptogram is exactly eight bytes and encoded per EMV specifications tag '9F26'.
     * <p>
     * All input fields are mandatory if any input field is missing or otherwise unavailable, then the Transaction Identifier is not calculated.
     * Example
     * Given:
     * pan (compressed numeric value with only a single trailing 'F' nibble and no excessive trailing 'FF' bytes, giving an 8-byte hex value): 123456789012345F
     * atc (hex value): 0001
     * applicationCryptogram (hex value): 1122334455667788
     * Then:
     * Concatenate the three byte arrays together, giving an 18-byte hex value of 123456789012345F00011122334455667788
     * SHA-256 hash the 18-byte value to give a Transaction Identifier value of 94e09c05c05aa8d183d14aeac628ebb7c0325e80881811f2ac53e81db86eb0b6
     *
     * @param pan
     * @param atc
     * @param applicationCryptogram
     */
    public static byte[] buildForDe55(byte[] pan, byte[] atc, byte[] applicationCryptogram) {
        String panStr = MchipUtils.byteArrayToHexString(pan);
        while (panStr.endsWith("FF")) {
            panStr = panStr.substring(0, panStr.length() - 1);
        }
        byte[] panBinary = MchipUtils.byteArrayFromHexString(panStr);
        if ((panBinary[0] >> 4) == 0) {
            BigInteger bigInteger = new BigInteger(panBinary);
            panBinary = bigInteger.shiftLeft(4).toByteArray();
            panBinary[panBinary.length - 1] |= 0x0F;
        }

        MchipUtils.ByteArrayBuilder builder = new MchipUtils.ByteArrayBuilder()
                .add(panBinary)
                .add(atc)
                .add(applicationCryptogram);

        return CryptoUtils.sha256(builder.build());
    }
}