import commandFactory.GetData;
import commandFactory.ReadRecord;
import mchipengine.utils.MchipUtils;
import models.Date;
import models.TlvEditor;
import wallet.SEChannel;

/**
 * The McmProfileBuilder provides a builder object that offers a service to easily create a structure of type McmProfile.
 */
public final class McmProfileBuilder {

    protected byte[] mAid;
    protected byte[] mAip = null;
    protected byte[] mAfl = null;
    protected byte[] mPdol = null;
    protected byte[] mPan = null;
    protected byte mPanSequenceNumber = 0x00;
    protected byte[] mPar = null;
    protected byte[] mTrack2EquivalentData = null;
    protected byte[] mLanguagePreference = null;
    protected Date mApplicationEffectiveDate = null;
    protected Date mExpirationDate = null;
    protected byte[] mCardCountryCode = null;
    protected byte[] mTokenRequestorId = null;
    protected byte[] mLast4DigitsOfPan = null;
    protected byte[] mApplicationVersionNumber = null;
    protected byte mCdol1RelatedDataLength = 0x00;
    protected boolean mIsRemotePaymentSupported = false;

    /**
     * mMcmAid= mcmAid
     *
     * @param mcmAid
     */
    public McmProfileBuilder(byte[] mcmAid) {
        this.mAid = mcmAid;
    }

    /**
     * Return an object of type McmProfile.
     * if      McmProfile.isMcmProfileValid returns false {
     * Throw IllegalStateException
     * }
     */
    public McmProfile build() {
        //MCM.1
        SEChannel channel = null; // TODO FIX

        //MCM.2
        byte[] fci = channel.getSelectResponse();
        TlvEditor tlvFci = TlvEditor.of(fci);

        //MCM.3
        GetData getData = new GetData(new byte[]{(byte) 0xDF, (byte) 0x30});
        byte[] supportedModes = channel.transmit(getData.getCommandApdu());
        // TODO: Check two last bytes for possible exception outcome --> Should end with 0x90 0x00
        mIsRemotePaymentSupported = (supportedModes & ((byte) 0x04)) == 0x04;

        if (mIsRemotePaymentSupported) {
            //MCM.7
            getData = new GetData(new byte[]{(byte) 0xDF, (byte) 0x31});
            mAip = channel.transmit(getData.getCommandApdu());

            //MCM.8
            getData = new GetData(new byte[]{(byte) 0xDF, (byte) 0x23});
            mAfl = channel.transmit(getData.getCommandApdu());

            //MCM.9
            getData = new GetData(new byte[]{(byte) 0xDF, (byte) 0x6A});
            // TODO: Check two last bytes for possible exception outcome --> Should end with 0x90 0x00
            mCdol1RelatedDataLength = channel.transmit(getData.getCommandApdu());

        } else {
            //MCM.4
            getData = new GetData(new byte[]{(byte) 0x00, (byte) 0x82});
            mAip = channel.transmit(getData.getCommandApdu());

            //MCM.5
            getData = new GetData(new byte[]{(byte) 0x00, (byte) 0x94});
            mAfl = channel.transmit(getData.getCommandApdu());

            //MCM.6
            getData = new GetData(new byte[]{(byte) 0x00, (byte) 0xC7});
            // TODO: Check two last bytes for possible exception outcome --> Should end with 0x90 0x00
            mCdol1RelatedDataLength = channel.transmit(getData.getCommandApdu());
        }

        //MCM.10
        // TODO parse AFL as TLV
        // For each record, do
        while (???){
            byte[] recordArray = new byte[ ??];
            ReadRecord readRecord = new ReadRecord(record, sfi);
            recordArray = channel.transmit(readRecord.getCommandApdu());
        }

        //MCM.11
        channel.close();

        //MCM.12
        TlvEditor tlvRecordArray = TlvEditor.of(recordArray);
        mPdol = tlvFci.getValue(new byte[]{(byte) 0x9F, (byte) 0x38});
        mLanguagePreference = tlvFci.getValue(new byte[]{(byte) 0x5F, (byte) 0x2D});
        mPan = tlvFci.getValue(new byte[]{(byte) 0x5A});
        mPanSequenceNumber = tlvFci.getValue(new byte[]{(byte) 0x5F, (byte) 0x34});
        mPar = tlvFci.getValue(new byte[]{(byte) 0x9F, (byte) 0x24});
        mTrack2EquivalentData = tlvFci.getValue(new byte[]{(byte) 0x57});
        byte[] effectiveDate = tlvFci.getValue(new byte[]{(byte) 0x5F, (byte) 0x25});
        mApplicationEffectiveDate = new Date(
                MchipUtils.bcdByteToInt(effectiveDate[0]),
                MchipUtils.bcdByteToInt(effectiveDate[1]),
                MchipUtils.bcdByteToInt(effectiveDate[2])
        );
        byte[] expirationDate = tlvFci.getValue(new byte[]{(byte) 0x5F, (byte) 0x24});
        mExpirationDate = new Date(
                MchipUtils.bcdByteToInt(expirationDate[0]),
                MchipUtils.bcdByteToInt(expirationDate[1]),
                MchipUtils.bcdByteToInt(expirationDate[2])
        );
        mCardCountryCode = tlvFci.getValue(new byte[]{(byte) 0x5F, (byte) 0x28});
        mTokenRequestorId = tlvFci.getValue(new byte[]{(byte) 0x9F, (byte) 0x19});
        mLast4DigitsOfPan = tlvFci.getValue(new byte[]{(byte) 0x9F, (byte) 0x25});
        mApplicationVersionNumber = tlvFci.getValue(new byte[]{(byte) 0x9F, (byte) 0x08});

        return new McmProfile(mAid,
                mAip,
                mAfl, mPdol, mPan, mPanSequenceNumber, mPar, mTrack2EquivalentData, mApplicationEffectiveDate,
                mExpirationDate, mCardCountryCode, mTokenRequestorId, mLast4DigitsOfPan, mApplicationVersionNumber,
                mCdol1RelatedDataLength, mIsRemotePaymentSupported);
    }

}