import commandFactory.ActivateMode;
import commandFactory.GenerateAc;
import commandFactory.GetProcessingOptions;
import exceptions.GeneralSecurityException;
import exceptions.Iso7816Exception;
import exceptions.NoValidCredentialsException;
import exceptions.TransactionDeclinedException;
import mchipengine.utils.MchipUtils;
import models.CryptogramInput;
import models.MchipCryptoOutcome;
import models.McmMode;
import models.TlvEditor;
import wallet.SEChannel;

import java.util.Arrays;

/**
 * A class responsible for building the DSRP cryptogram data field.
 * It also retrieves a TransactionId.
 */
public abstract class DsrpCryptoData {

    protected byte[] mMcmAid = null;

    /**
     * Holds the McmProfile
     */
    protected McmProfile mMcmProfile;

    /**
     * Provide the interfaces with the Wallet
     */
    protected Wallet mWallet;

    /**
     * Constructor
     * <p>
     * mMcmAid = mcmAid
     * mMcmProfile = mcmProfile
     * mWallet = wallet
     *
     * @param mcmAid
     * @param mcmProfile
     * @param wallet
     */
    public DsrpCryptoData(byte[] mcmAid, McmProfile mcmProfile, Wallet wallet) {
        this.mMcmAid = mcmAid;
        this.mMcmProfile = mcmProfile;
        this.mWallet = wallet;
    }

    /**
     * This method is abstract - it is defined by each subclass individually.
     * It returns a cryptogram data field according to the cryptogram type requested (UCAF_V0, UCAF_V0_PLUS, UCAF_V3 or DE55).
     *
     * @param cryptogramInput This is the class used to store all data elements required to compute an M/Chip cryptogram
     *                        Elements are prepared, based on type of cryptogram requested by the wallet.
     */
    public abstract byte[] buildCryptogramField(SEChannel channel, CryptogramInput cryptogramInput) throws NoValidCredentialsException, GeneralSecurityException, TransactionDeclinedException, Iso7816Exception;

    /**
     * This method is abstract - it is defined by each subclass individually.
     * It returns a TransactionID for the type of requested transaction.
     */
    public abstract byte[] getTransactionId();

    /**
     * A method responsible for computing the Application Cryptogram (AC) as well as setting values of the Card Verification Results and Issuer Application Data.
     * <p>
     * It creates a MchipCryptoOutcome object, containing the application cryptogram, as well as the ATC, CID, CVR and Issuer Application Data.
     *
     * @param mcmChannel
     * @param cryptogramInput
     */
    protected MchipCryptoOutcome generateMchipCryptoOutcome(SEChannel mcmChannel, CryptogramInput cryptogramInput) throws Iso7816Exception, TransactionDeclinedException {
        // MCHIP.1
        mcmChannel.openBasicChannel(mMcmAid);

        if (mMcmProfile.isRemotePaymentSupported()) {
            // MCHIP.2
            ActivateMode mode = new ActivateMode(McmMode.REMOTE_PAYMENT);
            mcmChannel.transmit(mode.getCommandApdu());
        }

        // MCHIP.3
        GetProcessingOptions gpo = new GetProcessingOptions(mMcmProfile.getPdol(), cryptogramInput.getTerminalCountryCode());
        byte[] gpoResponse = mcmChannel.transmit(gpo.getCommandApdu());

        if (gpoResponse[gpoResponse.length - 1] != 0x00 && gpoResponse[gpoResponse.length - 2] != 0x90) {
            throw new Iso7816Exception();
        }

        // MCHIP.4
        MchipUtils.ByteArrayBuilder cdol1RelatedData = new MchipUtils.ByteArrayBuilder();
        cdol1RelatedData.add(cryptogramInput.getAmount())
                .add(new byte[]{0x00, 0x00, 0x00, 0x00, 0x00, 0x00})
                .add(cryptogramInput.getTerminalCountryCode())
                .add(new byte[]{0x00, 0x00, 0x00, 0x00, 0x00})
                .add(cryptogramInput.getTransactionCurrencyCode())
                .add(cryptogramInput.getTransactionDate())
                .add(cryptogramInput.getTransactionType())
                .add(cryptogramInput.getUnpredictableNumber())
                .add(new byte[]{0x00})
                .add(new byte[]{0x00, 0x00})
                .add(new byte[]{0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00})
                .add(new byte[]{0x01, 0x00, 0x02})
                .add(new byte[]{0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
                        0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00});

        if (mMcmProfile.getCdol1RelatedDataLength() > 63) {
            while (cdol1RelatedData.size() < mMcmProfile.getCdol1RelatedDataLength()) {
                cdol1RelatedData.add((byte) 0x00);
            }
        }

        // MCHIP.5
        GenerateAc generateAc = new GenerateAc(cdol1RelatedData.build());
        byte[] genacResponse = mcmChannel.transmit(generateAc.getCommandApdu());


        if (genacResponse[genacResponse.length - 1] != 0x00 && genacResponse[genacResponse.length - 2] != 0x90) {
            throw new Iso7816Exception();
        }
        // MCHIP.6
        mcmChannel.close();

        // MCHIP.7
        TlvEditor tlvGenAc = TlvEditor.of(genacResponse);
        byte[] atc = tlvGenAc.getValue("9F36");
        byte[] ac = tlvGenAc.getValue("9F26");
        byte cid = tlvGenAc.getValue("9F27")[0];
        byte[] iad = tlvGenAc.getValue("9F10");
        byte[] cvr = Arrays.copyOfRange(iad, 2, 7);
        MchipCryptoOutcome mchipCryptoOutcome = new MchipCryptoOutcome(
                atc, cid, cvr, ac, iad
        );

        if (mchipCryptoOutcome.getCid() == 0x80) {
            throw new TransactionDeclinedException();
        }

        return mchipCryptoOutcome;
    }

}