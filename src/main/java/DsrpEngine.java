import exceptions.*;
import mchipengine.utils.MchipUtils;
import models.*;

import java.util.Arrays;

/**
 * DsrpEngine is the class used to provide the main interface for performing DSRP transactions.
 * <p>
 * Upon a call to the DsrpEngine, using the method processDsrpTransaction(), the DsrpEngine will:
 * * Perform a card profile check to determine whether the card profile of the M/Chip Mobile application onto the SE is still valid
 * * Perform a format check of the input data provided in the DsrpInputData structure
 * * check if user has been authenticated and his consent has been given for this transaction
 * * Build and return a DsrpOutputData structure in case the transaction is successful
 * <p>
 * There is a one-to-one relationship between an instance of DsrpEngine, and an instance of M/Chip Mobile application onto the Secure Element
 */
public class DsrpEngine {

    private byte[] mMcmAid = null;
    /**
     * Holds the McmProfile
     */
    private McmProfile mMcmProfile;
    /**
     * Provide the interfaces with the Wallet
     */
    private Wallet mWallet;

    /**
     * Constructor.
     * <p>
     * if      mcmProfile.isProfileValid() == false
     * {
     * Throw InvalidProfileException()
     * }
     * else {
     * mMcmAid = mcmAid
     * mMcmProfile = mcmProfile
     * mWallet = wallet
     * }
     *
     * @param mcmAid
     * @param mcmProfile
     * @param wallet
     */
    public DsrpEngine(byte[] mcmAid, McmProfile mcmProfile, Wallet wallet) throws InvalidProfileException {
        if (!mcmProfile.isMcmProfileValid()) {
            throw new InvalidProfileException();
        } else {
            this.mMcmAid = mcmAid;
            this.mMcmProfile = mcmProfile;
            this.mWallet = wallet;
        }
    }

    /**
     * Returns a structure of type DsrpOutputData, that contains either a Data Element 55 (DE 55 - chip data) field or a UCAF field for a Digital Secure Remote Payment (DSRP) transaction (DE48, Subelement 43).
     * In case of a UCAF field, the format used (UCAF Format 0, 0+ or 3) depends on the CryptogramType requested by the wallet.
     *
     * @param dsrpInputData
     */
    public DsrpOutputData processDsrpTransaction(DsrpInputData dsrpInputData) throws UserNotAuthenticatedException, IllegalArgumentException, CardNotValidException, NoValidCredentialsException, GeneralSecurityException {
        // DSRP.1
        boolean isCardValid;

        Date effectiveDate = mMcmProfile.getApplicationEffectiveDate();
        Date expirationDate = mMcmProfile.getExpirationDate();
        Date today = new Date();

        if (today.compareTo(expirationDate) < 0 || today.compareTo(effectiveDate) > 0) {
            isCardValid = false;
        } else {
            isCardValid = true;
        }

        if (!isCardValid) {
            throw new CardNotValidException();
        }

        if (dsrpInputData == null || !dsrpInputData.isDsrpInputDataValid()) {
            throw new IllegalArgumentException();
        }

        if (!mWallet.isConsentGiven() || !mWallet.isAuthenticated()) {
            throw new UserNotAuthenticatedException();
        }

        // DSRP.2
        CryptogramInput cryptogramInput = CryptogramInput.forDsrp(dsrpInputData, mMcmProfile.getAip());

        // DSRP.3
        byte[] merchantId;
        if (dsrpInputData.getMerchantId() == null || "".equals(dsrpInputData.getMerchantId())) {
            merchantId = null;
        } else {
            merchantId = dsrpInputData.getMerchantId().getBytes();
            if (merchantId.length > 111) {
                merchantId = Arrays.copyOfRange(merchantId, 0, 111);
            }
        }
        DsrpOutputData dsrpOutputData = buildDsrpOutputData(cryptogramInput, dsrpInputData.getCryptogramType(), merchantId);

        return dsrpOutputData;
    }

    /**
     * Build and return a structure of type DsrpOutputData
     *
     * @param cryptogramInput This is the class used to store all data elements required to compute an M/Chip cryptogram
     *                        Elements are prepared, based on type of cryptogram requested by the wallet.
     * @param cryptogramType  Indicate what type of cryptogram data format must be returned (UCAF Version 0, UCAF Version 0+, UCAF Version 1, DE55).
     * @param merchantId      The Merchant ID to be used during the cryptogram computation
     */
    private DsrpOutputData buildDsrpOutputData(CryptogramInput cryptogramInput, CryptogramType cryptogramType, byte[] merchantId) throws NoValidCredentialsException, GeneralSecurityException {
        // OUT.1
        byte[] profilePan = mMcmProfile.getPan();
        String pan = MchipUtils.byteArrayToHexString(profilePan);
        pan = removeFPadding(pan);

        // OUT.2
        byte profilePanSequenceNumber = mMcmProfile.getPanSequenceNumber();
        int panSequenceNumber = profilePanSequenceNumber;

        // OUT.3
        Date expirationDate = mMcmProfile.getExpirationDate();

        // OUT.4
        byte[] profilePar = mMcmProfile.getPar();
        String par = MchipUtils.byteArrayToHexString(profilePar);

        // OUT.5
        byte[] profileTrack2EquivalentData = mMcmProfile.getTrack2EquivalentData();
        String track2EquivalentData = MchipUtils.byteArrayToHexString(profileTrack2EquivalentData);
        track2EquivalentData = removeFPadding(track2EquivalentData);

        DsrpCryptoData dsrpCryptoData = null;
        switch (cryptogramType) {
            case UCAF_V0:
            case UCAF_V0_PLUS:
                // OUT.7
                dsrpCryptoData = new Ucaf(mMcmAid, mMcmProfile, mWallet, cryptogramType);
                break;
            case UCAF_V3:
                // OUT.8
                dsrpCryptoData = new UcafV3(mMcmAid, mMcmProfile, mWallet, merchantId);
                break;
            case DE55:
                // OUT.9
                dsrpCryptoData = new De55(mMcmAid, mMcmProfile, mWallet);
                break;
        }

        byte[] cryptogramData = dsrpCryptoData.buildCryptogramField(cryptogramInput);

        byte[] transactionId = dsrpCryptoData.getTransactionId();

        return new DsrpOutputData(pan, panSequenceNumber, par, expirationDate, track2EquivalentData, cryptogramData, cryptogramType);
    }

    private String removeFPadding(String data) {
        if (data.length() % 2 == 1) {
            while (data.endsWith("F")) {
                data = data.substring(0, data.length() - 1);
            }
        }
        return data;
    }

}