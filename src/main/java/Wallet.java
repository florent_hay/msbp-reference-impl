/**
 * This interface is implemented by the wallet to provide DsrpEngine  with a callback to request from the wallet the status of CDCVM and consent for each transaction before approving it.
 */
public interface Wallet {

	/**
	 * This function indicates whether the Wallet CdCvmManager has performed Consumer Device Cardholder Verification Method according to the wallet CDCVM model and in compliance with the M/Chip Mobile requirements (See Mastercard SE-Based Payments Product Guide).  
	 * This function should return TRUE if any form of CDCVM has been performed (e.g. fingerprint) in compliance with the MSBP requirements.  
	 * Otherwise FALSE should be returned.  
	 */
	boolean isAuthenticated();

	/**
	 * This function indicates whether the WalletConsentManager has received the consent from the user to perform the transaction
	 * This function should return TRUE if the user consent has been given
	 * It can be set to FALSE otherwise.
	 *  
	 */
	boolean isConsentGiven();

}