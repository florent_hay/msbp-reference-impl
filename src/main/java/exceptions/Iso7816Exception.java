package exceptions;

/**
 * Exception thrown when the M/Chip Mobile application on the Secure Element returns an error on a GPO command or GENERATE AC command
 */
public class Iso7816Exception extends Exception {

}