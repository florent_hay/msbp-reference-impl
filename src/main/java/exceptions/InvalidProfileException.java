package exceptions;

/**
 * Exception thrown in case the CardProfile is not valid.
 */
public class InvalidProfileException extends Exception {

    public InvalidProfileException() {
    }

}