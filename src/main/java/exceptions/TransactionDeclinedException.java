package exceptions;

/**
 *  
 * Exception thrown when the DsrpEngine rejects a DSRP transaction request.
 */
public class TransactionDeclinedException extends Exception{

}