import exceptions.GeneralSecurityException;
import exceptions.Iso7816Exception;
import exceptions.NoValidCredentialsException;
import exceptions.TransactionDeclinedException;
import mchipengine.utils.ByteUtils;
import mchipengine.utils.MchipUtils;
import models.CryptogramInput;
import models.CryptogramType;
import models.MchipCryptoOutcome;
import wallet.SEChannel;

/**
 * A class responsible for building a UCAF V0 and V0+ field.
 * <p>
 * It also retrieves a TransactionId.
 */
public class Ucaf extends DsrpCryptoData {

    /**
     * Holds the binary UCAF field.
     */
    protected byte[] mBinaryUcafField = null;

    /**
     * Holds the cryptogram data format that must be returned (UCAF Version 0, UCAF Version 0+, UCAF Version 3, DE55)
     */
    protected CryptogramType mCryptogramType;

    public void setBinaryUcafField(byte[] mBinaryUcafField) {
        this.mBinaryUcafField = mBinaryUcafField;
    }

    /**
     * Constructor.
     * <p>
     * super(mcmcAid, mcmProfile, wallet);
     * mCryptogramType = cryptogramType;
     *
     * @param mcmAid
     * @param mcmProfile
     * @param wallet
     * @param cryptogramType Indicate what type of cryptogram data format must be returned (UCAF Version 0, UCAF Version 0+, UCAF Version 3, DE55)
     */
    public Ucaf(byte[] mcmAid, McmProfile mcmProfile, Wallet wallet, CryptogramType cryptogramType) {
        super(mcmAid, mcmProfile, wallet);
        this.mCryptogramType = cryptogramType;
    }

    /**
     * Return a UCAF field according to the version of UCAF requested (V0 or V0+). The result will be stored in cryptogramData.
     *
     * @param cryptogramInput This is the class used to store all data elements required to compute an M/Chip cryptogram
     *                        Elements are prepared, based on type of cryptogram requested by the wallet.
     */
    public byte[] buildCryptogramField(SEChannel mcmChannel, CryptogramInput cryptogramInput) throws NoValidCredentialsException, GeneralSecurityException, TransactionDeclinedException, Iso7816Exception {
        // UCAF0.1
        cryptogramInput.setAmountAuthorized(new byte[]{0x00, 0x00, 0x00, 0x00, 0x00, 0x00});
        cryptogramInput.setTerminalCountryCode(new byte[]{0x00, 0x00});
        cryptogramInput.setTransactionCurrencyCode(new byte[]{0x00, 0x00});
        cryptogramInput.setTransactionDate(new byte[]{0x00, 0x00, 0x00});
        cryptogramInput.setTransactionType((byte) 0x00);

        MchipCryptoOutcome mchipCryptoOutcome = generateMchipCryptoOutcome(mcmChannel, cryptogramInput);

        // UCAF0.2
        byte panSequenceNumber = mMcmProfile.getPanSequenceNumber();
        byte ucafPanSequenceNumber = (byte) ByteUtils.binaryStringToByteArray("0000" + ByteUtils.byteToBinaryString(panSequenceNumber).substring(4, 8))[0];
        MchipUtils.ByteArrayBuilder cryptogramData = new MchipUtils.ByteArrayBuilder();
        cryptogramData.add((byte) 0x00).add((byte) 0x00).add(panSequenceNumber);

        // UCAF0.3
        byte[] ac = mchipCryptoOutcome.getApplicationCryptogram();
        cryptogramData.add(ac);

        // UCAF0.4
        byte[] atc = mchipCryptoOutcome.getAtc();
        cryptogramData.add(atc);

        // UCAF0.5
        byte[] un = cryptogramInput.getUnpredictableNumber();
        cryptogramData.add(un);

        // UCAF0.6
        byte[] aip = mMcmProfile.getAip();
        cryptogramData.add(aip);

        // UCAF0.7
        byte[] issuerApplicationData = mchipCryptoOutcome.getIssuerApplicationData();
        byte keyDerivationIndex = issuerApplicationData[0];
        cryptogramData.add(keyDerivationIndex);

        // UCAF0.8
        byte cryptogramVersionNumber = issuerApplicationData[1];
        cryptogramData.add(cryptogramVersionNumber);

        // UCAF0.9
        if (mCryptogramType == CryptogramType.UCAF_V0_PLUS) {
            cryptogramData.add(mchipCryptoOutcome.getCvr());
        }

        // UCAF0.10
        // Useless as already built in cryptogramData;

        // UCAF0.11
        return MchipUtils.byteArrayToHexString(cryptogramData.build()).getBytes();
    }

    /**
     * Return the Transaction ID, calculated for the current transaction:
     * <p>
     * byte[] pan = mMcmProfile.getPan()
     * <p>
     * byte[] transactionId = TransactionId.buildForUcaf(pan, mBinaryUcafField)
     * <p>
     * return transactionId
     */
    public byte[] getTransactionId() {
        return TransactionId.buildForUcaf(mMcmProfile.getPan(), mBinaryUcafField);
    }

}