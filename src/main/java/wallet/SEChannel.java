package wallet;

public interface SEChannel {

    void openBasicChannel(byte[] mcmAid);

    byte[] getSelectResponse();
    
    byte[] transmit(byte[] commandApdu);

    void close();

}
