import exceptions.GeneralSecurityException;
import exceptions.Iso7816Exception;
import exceptions.NoValidCredentialsException;
import exceptions.TransactionDeclinedException;
import mchipengine.utils.*;
import models.CryptogramInput;
import models.CryptogramType;
import models.MchipCryptoOutcome;
import wallet.SEChannel;

import java.math.BigInteger;
import java.util.Arrays;

/**
 * A class responsible for building a UCAF V3 field.
 * It also retrieves a TransactionId.
 */
public class UcafV3 extends Ucaf {

    private byte[] mMerchantId;

    /**
     * Constructor.
     * <p>
     * super(mcmcAid, mcmProfile, wallet);
     *
     * @param mcmAid
     * @param mcmProfile
     * @param wallet
     * @param merchantId
     */
    public UcafV3(byte[] mcmAid, McmProfile mcmProfile, Wallet wallet, byte[] merchantId) {
        super(mcmAid, mcmProfile, wallet, CryptogramType.UCAF_V3);
        this.mMerchantId = merchantId;
    }

    /**
     * Return a UCAF field version 3. The result will be stored in cryptogramData.
     *
     * @param cryptogramInput This is the class used to store all data elements required to compute an M/Chip cryptogram
     *                        Elements are prepared, based on type of cryptogram requested by the wallet.
     */
    public byte[] buildCryptogramField(SEChannel mcmChannel, CryptogramInput cryptogramInput) throws NoValidCredentialsException, GeneralSecurityException, TransactionDeclinedException, Iso7816Exception {
        byte[] approximateAmount = compressAmount(cryptogramInput.getAmount());

        // UCAF2.1
        byte[] atc = mchipCryptoOutcome.getAtc();
        atc = atc + 1;
        byte[] merchantIdHash = computeMerchantIdSaltedHash(approximateAmount, atc);

        // UCAF2.2
        MchipUtils.ByteArrayBuilder amountAuthorizedBuilder = new MchipUtils.ByteArrayBuilder();
        amountAuthorizedBuilder.add(approximateAmount).add(merchantIdHash).add((byte) 0x00);
        byte[] amountAuthorized = amountAuthorizedBuilder.build();
        cryptogramInput.setAmountAuthorized(amountAuthorized);
        cryptogramInput.setTerminalCountryCode(new byte[]{0x00, 0x00});
        cryptogramInput.setTransactionCurrencyCode(new byte[]{0x00, 0x00});
        cryptogramInput.setTransactionDate(new byte[]{0x00, 0x00, 0x00});
        cryptogramInput.setTransactionType((byte) 0x00);

        MchipCryptoOutcome mchipCryptoOutcome = generateMchipCryptoOutcome(mcmChannel, cryptogramInput);

        // UCAF2.3
        byte[] intermediateAmount;
        BigInteger bi = new BigInteger(1, approximateAmount);
        intermediateAmount = bi.shiftLeft(1).toByteArray();
        if (ByteUtils.byteArrayToLong(intermediateAmount) > 0) {
            intermediateAmount = ByteUtils.removeLeadingZeros(intermediateAmount);
        }
        byte[] approximateAmountPart1 = Arrays.copyOfRange(intermediateAmount, 0, 2);

        byte[] approximateAmountPart2;
        if (mWallet.isAuthenticated()) {
            approximateAmountPart2 = new byte[]{(byte) (approximateAmount[2] | 0x80)};
        } else {
            approximateAmountPart2 = new byte[]{(byte) (approximateAmount[2] & 0x7F)};
        }

        // UCAF2.4
        byte panSequenceNumber = mMcmProfile.getPanSequenceNumber();
        MchipUtils.ByteArrayBuilder ucafBuilder = new MchipUtils.ByteArrayBuilder();
        ucafBuilder
                .add((byte) ByteUtils.binaryStringToByteArray("0011" + ByteUtils.byteToBinaryString(panSequenceNumber).substring(4, 8))[0])
                .add(Arrays.copyOfRange(mchipCryptoOutcome.getApplicationCryptogram(), 0, 4))
                .add(merchantIdHash)
                .add(approximateAmountPart1)
                .add(Arrays.copyOfRange(mchipCryptoOutcome.getAtc(), 0, 2))
                .add(cryptogramInput.getUnpredictableNumber())
                .add(cryptogramInput.getAip())
                .add(Arrays.copyOfRange(this.mMcmProfile.getIssuerApplicationData(), 0, 2))
                .add(approximateAmountPart2);

        mBinaryUcafField = ucafBuilder.build();

        // UCAF2.5
        return ByteUtils.byteArrayToBase64String(mBinaryUcafField).getBytes();
    }

    /**
     * byte[] merchantIdHash = new byte[] {0x00, 0x00}
     * if merchantId == null {
     * return merchantIdHash
     * }
     * else {
     * byte[] dataToHash = CONCATE(mMerchantId || approximateAmount || atc)
     * byte[] hash = SHA-256(dataToHash)
     * merchantIdHash= hash[0 : 1]
     * return merchantIdHash
     * }
     * <p>
     * The SHA-256 algorithm is standardized in ISO/IEC 10118-3.
     * SHA-256 takes as input messages of arbitrary length and produces a 32-byte hash value.
     *
     * @param approximateAmount
     * @param atc
     */
    private byte[] computeMerchantIdSaltedHash(byte[] approximateAmount, byte[] atc) {
        byte[] merchantIdHash = new byte[]{0x00, 0x00, 0x00};
        if (mMerchantId == null) {
            return merchantIdHash;
        } else {
            byte[] dataToHash = ByteUtils.concatenateByteArrays(
                    Arrays.asList(merchantIdHash, approximateAmount, atc)
            );
            byte[] hash = CryptoUtils.sha256(dataToHash);
            merchantIdHash = Arrays.copyOfRange(hash, 0, 2);
            return merchantIdHash;
        }
    }

    /**
     * This method returned an amount compressed, used in the cryptogram generation of a UCAF V3.
     * <p>
     * Examples
     * Amount = 999999999999 (9 999 999 999,99 Euro)
     * 1110 1000 1101 0100 10 10 0101 0000 1111 1111 1111
     * X = 1110 1000 1101 0100 10
     * Y = 10 0101 0000 1111 1111 1111
     * A = 1110 1000 1101 0100 11, B= 1 0110, representing 10 000 017 653, 76 Euro
     * <p>
     * Amount = 262143 (2 621,43 Euro)
     * 11 1111 1111 1111 1111
     * A = 11 1111 1111 1111 1111, B = 0 0000, representing 2 621,43 Euro
     * <p>
     * Amount = 2621440 (26 214,40 Euro)
     * 10 1000 0000 0000 0000 0000
     * X = 10 1000 0000 0000 0000
     * Y = 0000
     * A = 10 1000 0000 0000 0000, B= 00100, representing 26 214,40 Euro
     * <p>
     * Amount = 2621441 (26 214,41 Euro)
     * 10 1000 0000 0000 0000 0001
     * X = 10 1000 0000 0000 0000
     * Y = 0001
     * A = 10 1000 0000 0000 0001, B= 00100, representing 26 214, 56 Euro
     * <p>
     * Amount = 262145 (2 621,45 Euro)
     * 100 0000 0000 0000 0001
     * X = 100 0000 0000 0000 000
     * Y = 1
     * A = 100 0000 0000 0000 001, B= 1, representing 2 621,46 Euro
     *
     * @param amount
     */
    private byte[] compressAmount(byte[] amount) {
        byte[] approximateAmount = new byte[]{0x00, 0x00, 0x00};
        //String compressedAmount = "00000000000000000000000b";

        if (Arrays.equals(amount, new byte[]{0x00, 0x00, 0x00, 0x00, 0x00, 0x00})) {
            return approximateAmount;
        }

        String amountValue = BcdUtils.bcdToBinaryString(amount);
        long amountNumber = Long.parseLong(amountValue, 2);

        String x;
        String y;
        String a = amountValue;
        String b = "0";
        if (amountNumber > Long.parseLong("111111111111111111", 2)) {
            x = amountValue.substring(0, 18);
            y = amountValue.substring(18);

            if (Integer.valueOf(y, 2) == 0) {
                a = x;
                b = Integer.toBinaryString(y.length());
            } else {
                if (x.equals("111111111111111111")) {
                    a = "100000000000000000";
                    b = Integer.toBinaryString(y.length() + 1);
                } else {
                    a = Integer.toBinaryString(Integer.parseInt(x, 2) + 1);
                    b = Integer.toBinaryString(y.length());
                }
            }
        }

        a = StringUtils.padFixed(a, "0", 18, true);

        b = StringUtils.padFixed(b, "0", 5, true);

        String compressedAmount = "0" + a + b;

//		approximateAmount = BcdUtils.binaryStringToBcdByteArray(compressedAmount);
        approximateAmount = ByteUtils.binaryStringToByteArray(compressedAmount);

        return approximateAmount;
    }

}