import exceptions.GeneralSecurityException;
import exceptions.NoValidCredentialsException;
import models.*;

/**
 * A class responsible for building a DE55 field.
 * If the McmProfile supports the generation of a TransactionId, then the De55 class also retrieves a TransactionId.
 */
public class De55 extends DsrpCryptoData {

	/**
	 * Holds a link to the ATC.
	 */
	private byte[] mAtc = null;
	/**
	 * Holds the AC.
	 */
	private byte[] mApplicationCryptogram = null;

	/**
	 * Constructor.
	 * 
	 * super(mcmcAid, mcmProfile, wallet);
	 * @param mcmAid
	 * @param mcmProfile
	 * @param wallet
	 */
	public De55(byte[] mcmAid, McmProfile mcmProfile, Wallet wallet) {
		super(mcmAid, mcmProfile, wallet);
	}

	/**
	 * Return an array of bytes CryptogramData:
	 * Create a byte array CryptogramData and concatenate the Tag, Length and Value of the Data Elements presented in the table below; and return the resulting binary buffer:
	 * 
	 * Tag
	 * Description
	 * Value
	 * '9F26'
	 * Application Cryptogram
	 * MchipCryptoOutcome.getApplicationCryptogram()
	 * '9F10'
	 * Issuer Application Data
	 * MchipCryptoOutcome.getIssuerApplicationData()
	 * '9F36'
	 * Application Transaction Counter
	 * MchipCryptoOutcome.getAtc()
	 * '95'
	 * Terminal Verification Results
	 * {0x00, 0x00, 0x00, 0x00, 0x00}
	 * '9F27'
	 * Cryptogram Information Data
	 * MchipCryptoOutcome.getCid()
	 * '9F34'
	 * CVM Results
	 * {0x01, 0x00, 0x02}
	 * '9F37'
	 * Unpredictable Number
	 * cryptogramInput.getUnpredictableNumber()
	 * '9F02'
	 * Amount, Authorized (Numeric)
	 * cryptogramInput.getAmount()
	 * '9F03'
	 * Amount, Other (Numeric)
	 * {0x00, 0x00, 0x00, 0x00, 0x00, 0x00}
	 * '5F2A'
	 * Transaction Currency Code
	 * cryptogramInput.getCurrencyCode()
	 * '9A'
	 * Transaction Date
	 * cryptogramInput.getTransactionDate()
	 * '9C'
	 * Transaction Type
	 * cryptogramInput.getTransactionType()
	 * '5A'
	 * PAN
	 * mMcmProfile.getPan()
	 * '5F34'
	 * PAN Sequence Number
	 * mMcmProfile.getPanSequenceNumber()
	 * '5F24'
	 * Expiration Date
	 * mMcmProfile.getExpirationDate()
	 * '9F1A'
	 * Terminal Country Code
	 * mMcmProfile.getCountryCode()
	 * '82'
	 * Application Interchange Profile
	 * mMcmProfile.getAip()
	 *  
	 * @param cryptogramInput This is the class used to store all data elements required to compute an M/Chip cryptogram  
	 * Elements are prepared, based on type of cryptogram requested by the wallet.
	 */
	public byte[] buildCryptogramField(CryptogramInput cryptogramInput) throws NoValidCredentialsException, GeneralSecurityException {
		// TODO - implement De55.buildCryptogramField
		throw new UnsupportedOperationException();
	}

	/**
	 * Return the Transaction ID, calculated for the current transaction:
	 * 
	 * byte[] pan = mMcmProfile.getPan()
	 * 
	 * byte[] transactionId = TransactionId.buildForDe55(pan, mAtc, mApplicationCryptogram)
	 * 
	 * return transactionId
	 */
	public byte[] getTransactionId() {
		// TODO - implement De55.getTransactionId
		throw new UnsupportedOperationException();
	}

}