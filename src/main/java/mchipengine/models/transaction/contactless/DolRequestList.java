package mchipengine.models.transaction.contactless;

import mchipengine.utils.MchipUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * Manages a Data Object List (DOL) as requested by mchipengine to the contactless terminal.
 * mchipengine manages several DOLs that are inserted as a BER-TLV data object in specific R-APDUs to request information from the terminal:
 * The PDOL that is requested to the terminal in the Select R-APDU. The result is transmitted by the terminal in the Get Processing Options C-APDU.
 * The UDOL that is requested in the Read Record R-APDU for Record 1 SFI 1. The result is transmitted in the Compute Cryptographic Checksum C-APDU.
 * The CDOL1 that is requested in one of the Read Record R-APDUs. The result is transmitted in the Generate AC C-APDU.
 * Each item of a DOL request list is a pair {TAG, LENGTH} and corresponds to a terminal-based EMV data element whose VALUE will be returned by the terminal within a specific C-APDU of the transaction.
 */
public class DolRequestList {
    /**
     * Holds the DOL as a list of DolItems.
     */
    private List<DolItem> mDolItems = new ArrayList<>();

    /**
     * Public static factory method, creates a DolRequestList object that contains an ordered list of pairs {Tag, Length}.
     *
     * @param dolList The DOL list in the form of Tag || Length || values
     * @return A DOL management object that can be used to manipulate the list
     */
    public static DolRequestList of(final byte[] dolList) {
        return new DolRequestList(dolList);
    }

    /**
     * Returns the list of DolItems (Java List) that constitute the Data Object List (DOL)
     *
     * @return The list of DOLs
     */
    public final DolItem[] getRequestList() {
        return mDolItems.toArray(new DolItem[mDolItems.size()]);
    }

    /**
     * Appends a new DolItem {Tag, Length} to the DOL.
     * Returns TRUE if the new DolItem was successfully added to the DOL, FALSE if the DOL was already containing this tag.
     *
     * @param tag    The new tag to be appended to the DOL, for a request to the terminal.
     * @param length Expected length for the value of the new tag.
     */
    public boolean addTag(final String tag, final int length) {
        if (isValuePresentInDol(tag)) {
            return false;
        }
        mDolItems.add(new DolItem(tag, length));
        return true;
    }

    /**
     * Returns the expected length for the DOL Related Data (provided by the terminal), that is, the sum of all the lengths of all the DolItems from the DolRequestList.
     *
     * @return The expected length of the DOL data blob (in bytes)
     */
    public int getExpectedDolLength() {
        int result = 0;
        for (DolItem listItem : mDolItems) {
            result += listItem.mLength;
        }
        return result;
    }

    /**
     * Private constructor, parsing the input DOL buffer (tags, lengths) and creating a DolRequestList out of it.
     * This private constructor is not available, please use the static public factory of() method instead.
     *
     * @param dolList The DOL list as byte[]. It consists of the concatenation of {Tag, Length} couples
     */
    private DolRequestList(final byte[] dolList) {
        mDolItems = new ArrayList<>();
        if (dolList == null) {
            // It is an empty, we cannot add anything
            return;
        }
        int nextTagOffset = 0;
        while (nextTagOffset < dolList.length) {
            final byte[] nextTag = readNextTag(dolList, nextTagOffset);
            final String tag = MchipUtils.byteArrayToHexString(nextTag);
            final int lengthOffset = nextTagOffset + nextTag.length;
            final int noBytesInLength = getNumberOfBytesInLength(dolList, lengthOffset);
            final int nextDataLength = getDataLength(dolList, lengthOffset);

            nextTagOffset += nextTag.length + noBytesInLength;

            mDolItems.add(new DolItem(tag, nextDataLength));
        }
    }

    /**
     * Returns the value of the Length field of a specific DOL element.
     * The offset of the Length field in the DOL buffer must be specified.
     *
     * @param data         The byte array to be parsed
     * @param lengthOffset Offset of the Length field inside the data buffer.
     */
    private static int getDataLength(final byte[] data, final int lengthOffset) {
        if (data == null || lengthOffset < 0 || lengthOffset >= data.length) {
            throw new IllegalArgumentException("Invalid offset or data");
        }

        final int noBytesInLength = getNumberOfBytesInLength(data, lengthOffset);
        if (data.length < lengthOffset + noBytesInLength) {
            throw new IllegalArgumentException("Invalid length ");
        }
        if (noBytesInLength == 1) {
            return data[lengthOffset];
        }
        int length = 0;
        for (int i = 1; i < noBytesInLength; i++) {  // We start from the second byte of length
            final int shift = 8 * (noBytesInLength - i - 1);
            length += ((data[i + lengthOffset] & 0x00FF) << shift);
        }
        return length;
    }

    /**
     * Returns the DOL as a byte array, that is, a concatenation of [tag, length] couples.
     *
     * @return The PDOL List as byte[]
     */
    public byte[] getBytes() {
        // Get the DOL as a byte[]
        int totalLength = 0;
        for (DolItem item : mDolItems) {
            // In this case the maximum PDOL value length is never greater than 255
            totalLength += (item.mTag.length() / 2) + (item.mLength < 0x7F ? 1 : 2);
        }
        final byte[] pdol = new byte[totalLength];
        int currentPosition = 0;
        for (DolItem item : mDolItems) {
            final byte[] nextValue = MchipUtils.byteArrayFromHexString(item.mTag);
            System.arraycopy(nextValue, 0, pdol, currentPosition, nextValue.length);
            currentPosition += nextValue.length;

            final int lengthLength = item.mLength < 0x7F ? 1 : 2;
            if (lengthLength == 1) {
                pdol[currentPosition] = (byte) item.mLength;
            } else {
                pdol[currentPosition] = (byte) 0x81;
                currentPosition++;
                pdol[currentPosition] = (byte) item.mLength;
            }
            currentPosition++;
        }
        return pdol;
    }

    /**
     * Checks whether the DOL entry (Tag) is already present in the list.
     *
     * @param tag The Tag to search for in the Data Object List
     * @return True if the value is already in the list, false otherwise
     */
    public boolean isValuePresentInDol(final String tag) {
        for (DolItem dolItem : mDolItems) {
            if (dolItem.mTag.equalsIgnoreCase(tag)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Reads a tag value from a given byte array formatted as an EMV DOL, at the indicated offset position. It assumes the Tag field starts at the offset position. This method returns the Tag field.
     *
     * @param data   The input buffer, in BER-TLV format
     * @param offset the offset of the tag to be read
     * @return The Tag of the TLV
     * @throws IllegalArgumentException if the TLV is malformed
     */
    private static byte[] readNextTag(final byte[] data, final int offset) throws IllegalArgumentException {
        if (data == null || offset < 0 || offset >= data.length) {
            throw new IllegalArgumentException("Invalid TLV: "
                    + MchipUtils.byteArrayToHexString(data));
        }
        final int tagLength = getTagLength(data, offset);
        final byte[] tag = new byte[tagLength];
        System.arraycopy(data, offset, tag, 0, tagLength);
        return tag;
    }

    /**
     * Utility function that computes the length of the Tag field of a DOL element as per BER-TLV rules. The tag is assumed to start at 'offset' position within the data.
     *
     * @param data   The byte array to be parsed
     * @param offset The offset at which the tag is located
     * @return The length of the tag
     * @throws IllegalArgumentException If the data is malformed
     */
    private static int getTagLength(final byte[] data, final int offset) {
        if (data == null || offset < 0 || offset >= data.length) {
            throw new IllegalArgumentException("Invalid offset or data");
        }
        int tagLength = 1;
        if ((data[offset] & 0x1F) == 0x1F) {  // we need to look at the next byte
            for (int i = 1; i < data.length; i++) {
                tagLength++;
                if ((data[offset + i] & 0x80) != 0x80) {
                    break;
                }
            }
        }
        return tagLength;
    }

    /**
     * Utility function that computes the length of the Length field of a DOL element, as per BER-TLV rules. The length is assumed to start at 'offset' position within the DOL buffer.
     *
     * @param data         The buffer containing the TLV data where the Length field is to be parsed
     * @param lengthOffset Offset of the Length field inside the data buffer.
     */
    private static int getNumberOfBytesInLength(final byte[] data, final int lengthOffset) {
        if (data == null || lengthOffset < 0 || lengthOffset >= data.length) {
            throw new IllegalArgumentException("Invalid offset or data");
        }
        final byte firstLengthByte = data[lengthOffset];
        return 1 + ((firstLengthByte & 0x80) == 0x80 ? (firstLengthByte & 0x7F) : 0);
    }

    /**
     * This method is for debug purposes only. It returns a string containing the list of {Tag, Length} present in the DOL.
     */
    @Override
    public String toString() {
        final StringBuilder stringBuilder = new StringBuilder();
        for (DolItem dolItem : mDolItems) {
            stringBuilder.append(dolItem.mTag);
            stringBuilder.append(", ");
            stringBuilder.append(dolItem.mLength);
            stringBuilder.append("\n");
        }
        return stringBuilder.toString();
    }

    /**
     * Structure representing a single item in a Data Object List (DOL) requested to the POS by the Mobile Payment Application. A DOL item is represented by its tag and its length.
     */
    public static class DolItem {
        /**
         * The tag associated to the DOL item
         */
        private final String mTag;
        /**
         * The length associated to the DOL item
         */
        private final int    mLength;

        /**
         * @return The tag value
         */
        public String getTag() {
            return mTag;
        }

        /**
         * @return The tag length as integer
         */
        public int getLength() {
            return mLength;
        }

        /**
         * Build a DOL Item with a given tag and length
         *
         * @param tag    The tag associated to the DOL item
         * @param length The length associated to the DOL item
         */
        public DolItem(final String tag, final int length) {
            mTag = tag;
            mLength = length;
        }
    }
}