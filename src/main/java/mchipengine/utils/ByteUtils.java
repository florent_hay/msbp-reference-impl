package mchipengine.utils;

import java.util.List;

public class ByteUtils {
    public static byte[] concatenateByteArrays(List<byte[]> asList) {
        return new byte[0];
    }

    public static long byteArrayToLong(byte[] value) {
        return 0L;
    }

    public static byte[] removeLeadingZeros(byte[] data) {
        return new byte[0];
    }

    public static String byteArrayToBase64String(byte[] data) {
        return null;
    }

    public static String byteToBinaryString(byte data) {
        return null;
    }

    public static int[] binaryStringToByteArray(String data) {
        return new int[0];
    }
}
