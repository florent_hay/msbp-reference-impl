package mchipengine.utils;

import models.DolValues;

import java.util.ArrayList;
import java.util.List;

/**
 * Set of utility functions used by mchipengine.
 */
public class MchipUtils {

    /**
     * Hexadecimal string prefix.
     */
    private static final String HEX_PREFIX = "0x";

    /**
     * Concatenate the arrays in the list of arrays.
     */
    public static byte[] concatenateArrays(final List<byte[]> arrays) {
        int arrayLength = 0;
        for (byte[] item : arrays) {
            arrayLength += item.length;
        }
        final byte[] arrayData = new byte[arrayLength];

        int index = 0;
        for (byte[] item : arrays) {
            System.arraycopy(item, 0, arrayData, index, item.length);
            index += item.length;
        }
        return arrayData;

    }

    /**
     * Clear the arrays in the list of arrays.
     */
    public static void clearArraysList(final List<byte[]> arrays) {
        for (byte[] item : arrays) {
            clearByteArray(item);
        }
    }

    /**
     * Constructs a byte array from the given hexadecimal string.
     * The string may begin with the prefix '0x'.
     *
     * @param hexString the hexadecimal string.
     * @return a byte array. Never <code>null</code>.
     * @throws NumberFormatException if the string has invalid characters.
     */
    public static byte[] byteArrayFromHexString(String hexString) {
        if (hexString == null) {
            return null;
        }
        int len = hexString.length();
        if (len % 2 != 0) {
            hexString = "0" + hexString;
            len++;
        }
        return getBytes(hexString, len);
    }

    /**
     * Convert the byte array into a hex string.
     */
    public static String byteArrayToHexString(final byte[] data) {
        if (data == null) {
            return null;
        }
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < data.length; i++) {
            byte b = data[i];
            sb.append(String.format(i == 0 ? "%x" : "%02x", b));
        }
        return sb.toString().toUpperCase();
    }

    private static byte[] getBytes(String s, int len) {
        byte[] data = new byte[len / 2];
        for (int i = 0; i < len; i += 2) {
            data[i / 2] = (byte) ((Character.digit(s.charAt(i), 16) << 4)
                    + Character.digit(s.charAt(i + 1), 16));
        }
        return data;
    }

    /**
     * Convert a long into a Byte Array encoded with BCD
     * For example, longToBcdByteArray(1023, 6) is coded as 0x000000001023
     *
     * @param num  The long value to be converted to BCD Byte Array
     * @param size The number of bytes into which the result should be stored
     * @return A Byte Array BCD encoded
     */
    public static byte[] longToBcd(long num, int size) {
        int digits = 0;
        long temp = num;
        while (temp != 0) {
            digits++;
            temp /= 10;
        }

        int byteLen = digits % 2 == 0 ? digits / 2 : (digits + 1) / 2;
        boolean isOdd = digits % 2 != 0;
        byte bcd[] = new byte[byteLen];
        for (int i = 0; i < digits; i++) {
            byte tmp = (byte) (num % 10);
            if (i == digits - 1 && isOdd) {
                bcd[i / 2] = tmp;
            } else if (i % 2 == 0) {
                bcd[i / 2] = tmp;
            } else {
                byte foo = (byte) (tmp << 4);
                bcd[i / 2] |= foo;
            }
            num /= 10;
        }

        for (int i = 0; i < byteLen / 2; i++) {
            byte tmp = bcd[i];
            bcd[i] = bcd[byteLen - i - 1];
            bcd[byteLen - i - 1] = tmp;
        }
        if (size == byteLen) {
            return bcd;
        } else {
            byte[] ret = new byte[size];
            System.arraycopy(bcd, 0, ret, size - byteLen, byteLen);
            return ret;
        }
    }

    /**
     * Clear byte array.
     *
     * @param buffer the buffer
     */
    public static void clearByteArray(final byte[] buffer) {
        if (buffer == null) {
            return;
        }
        final int length = buffer.length;
        for (int i = 0; i < length; i++) {
            buffer[i] = (byte) 0;
        }
    }

    /**
     * Check whether an array is composed of all zeroes elements.
     *
     * @param data The input data as byte[]
     * @return true if all the elements are 0x00, false otherwise
     */
    public static boolean isZero(final byte[] data) {
        if (data == null) {
            throw new NullPointerException("Input data is null in isZero(...)");
        }
        for (byte elem : data) {
            if (elem != 0x00) {
                return false;
            }
        }
        return true;
    }

    /**
     * Writes a integer (4B) to the byte array.
     *
     * @param buffer a byte array.
     * @param offset defines the offset of the integer in the array.
     * @param value  the value to be written.
     */
    public static void writeInt(final byte[] buffer, final int offset, final long value) {
        buffer[offset] = (byte) ((value >> 24) & 0xFF);
        buffer[offset + 1] = (byte) ((value >> 16) & 0xFF);
        buffer[offset + 2] = (byte) ((value >> 8) & 0xFF);
        buffer[offset + 3] = (byte) (value & 0xFF);
    }

    /**
     * Convert a byte BCD encoded value into an integer
     *
     * @param input The BCD packed byte
     * @return the integer value representing the BCD packed byte
     */
    public static int bcdByteToInt(final byte input) {
        return (((input & 0x00F0) >> 4) * 10) + (input & 0x0F);
    }

    /**
     * Reads a (signed) short integer from the byte array.
     *
     * @param data         byte array containing the 2 integer bytes.
     * @param offset       beginning of the short integer value in the array.
     * @param littleEndian true if little endian byte order is used. If false then the integer is read using big endian.
     * @return the read short integer value.
     */
    private static int readShort(final byte[] data, final int offset, final boolean littleEndian) {
        if (littleEndian) {
            return ((data[offset + 1] << 8) | (data[offset] & 0xFF)) & 0xFFFF;
        } else {
            return ((data[offset] << 8) | (data[offset + 1] & 0xFF)) & 0xFFFF;
        }
    }

    /**
     * Reads a (signed) short integer from the byte array using big endian byte order.
     *
     * @param data   the data
     * @param offset the offset
     * @return the short
     */
    public static short readShort(final byte[] data, final int offset) {
        return (short) (readShort(data, offset, false));
    }

    /**
     * Returns the value of the Tag passed as an input parameter from the DOL values also provided in input.
     */
    public static byte[] getValueFromDols(DolValues pDolValues1, DolValues pDolValues2, String pTag) {
        if (pDolValues1 == null) {
            return null;
        }
        byte[] value = pDolValues1.getValueByTag(pTag);
        if (value == null) {
            if (pDolValues2 == null) {
                return null;
            }
            value = pDolValues2.getValueByTag(pTag);
        }

        return value;
    }


    public static class ByteArrayBuilder {
        private List<Byte> mBytes;

        public ByteArrayBuilder() {
            this.mBytes = new ArrayList<>();
        }

        public ByteArrayBuilder add(byte b) {
            mBytes.add(b);
            return this;
        }

        public ByteArrayBuilder add(byte[] bytes) {
            for (byte b : bytes) {
                mBytes.add(b);
            }
            return this;
        }

        public int size() {
            return mBytes.size();
        }

        public byte[] build() {
            final byte[] result = new byte[mBytes.size()];
            for (int i = 0; i < mBytes.size(); i++) {
                Byte b = mBytes.get(i);
                result[i] = b;
            }
            return result;
        }
    }
}