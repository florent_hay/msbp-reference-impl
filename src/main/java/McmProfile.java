import models.Date;

import java.util.Arrays;

/**
 * This class is the top level object used to represent the McmProfile object, as expected by the DsrpEngine object.
 */
public class McmProfile {

    private byte[] mAid;
    private byte[] mAip;
    private byte[] mAfl;
    private byte[] mPdol;
    private byte[] mPan;
    private byte mPanSequenceNumber;
    private byte[] mPar;
    private byte[] mTrack2EquivalentData;
    private byte[] mLanguagePreference;
    private Date mApplicationEffectiveDate;
    private Date mExpirationDate;
    private byte[] mCardCountryCode;
    private byte[] mTokenRequestorId;
    private byte[] mLast4DigitsOfPan;
    private byte[] mApplicationVersionNumber;
    private byte mCdol1RelatedDataLength;
    private boolean misRemotePaymentSupported;

    /**
     * Returns the AID of the paired M/Chip Mobile application instance on the SE.
     */
    public byte[] getAid() {
        return this.mAid;
    }

    /**
     * Returns the Application Interchange Profile (AIP) used for DSRP transactions.
     * <p>
     * The Application Interchange Profile indicates the capabilities of the application to support specific functions in the application when a DSRP transaction is processed.
     */
    public byte[] getAip() {
        return this.mAip;
    }

    public byte[] getPdol() {
        return mPdol;
    }

    /**
     * returns mAfl (the Application File Locator, which references the records used for DSRP transactions)
     */
    public byte[] getAfl() {
        return this.mAfl;
    }

    /**
     * returns mPan
     */
    public byte[] getPan() {
        return this.mPan;
    }

    /**
     * returns mPanSequenceNumber
     */
    public byte getPanSequenceNumber() {
        return this.mPanSequenceNumber;
    }

    /**
     * returns mPar
     */
    public byte[] getPar() {
        return this.mPar;
    }

    /**
     * returns mTrack2EquivalentData
     */
    public byte[] getTrack2EquivalentData() {
        return this.mTrack2EquivalentData;
    }

    /**
     * returns mLanguagePreference
     */
    public byte[] getLanguagePreference() {
        return this.mLanguagePreference;
    }

    /**
     * returns mApplicationEffectiveDate
     */
    public Date getApplicationEffectiveDate() {
        return this.mApplicationEffectiveDate;
    }

    /**
     * Return  mExpirationDate
     */
    public Date getExpirationDate() {
        return this.mExpirationDate;
    }

    /**
     * Return  m  Country Code ,  which is the country code   represented   according to [ISO3166-1] .
     */
    public byte[] getCardCountryCode() {
        return this.mCardCountryCode;
    }

    /**
     * Return the  Token Requestor ID .    The Token Requestor ID uniquely identifies each unique combination of Token Requestor and Token Domain(s) for each Registered Token Service Provider   according to [EMV-TOKEN]  .
     */
    public byte[] getTokenRequestorId() {
        return this.mTokenRequestorId;
    }

    /**
     * Returns the Application Label, which is a mnemonic associated with the AID according to ISO 7816-5.
     */
    public byte[] getApplicationLabel() {
        // TODO - implement McmProfile.getApplicationLabel
        throw new UnsupportedOperationException();
    }

    /**
     * Version number assigned by the payment system for the application. When not supported, the method returns null value.
     */
    public byte[] getApplicationVersionNumber() {
        return this.mApplicationVersionNumber;
    }

    /**
     * return mCdol1RelatedDataLength
     */
    public byte getCdol1RelatedDataLength() {
        return this.mCdol1RelatedDataLength;
    }

    /**
     * Return true if Modes Enabling Switch (tag 'DF30') bit 3 is set, false otherwise.
     */
    public boolean isRemotePaymentSupported() {
        return this.misRemotePaymentSupported;
    }

    /**
     * Return true if the McmProfile fetched from the MCM application is valid, false otherwise.
     */
    public boolean isMcmProfileValid() {
        return mAip != null
                && mAfl != null
                && mPan != null
                && mApplicationEffectiveDate != null
                && mApplicationEffectiveDate.isValid()
                && mExpirationDate.isValid()
                && mCardCountryCode != null
                && !Arrays.equals(mCardCountryCode, new byte[]{0x00, 0x00, 0x00})
                && mCdol1RelatedDataLength >= 0x3F;
    }

    /**
     * Constructor.
     *
     * @param aid
     * @param aip
     * @param afl
     * @param pan
     * @param psn
     * @param par
     * @param track2
     * @param language
     * @param effectiveDate
     * @param expirationDate
     * @param country
     * @param trid
     * @param last4digitsOfPan
     * @param avn
     * @param cdolLength
     * @param isRpSupported
     */
    public McmProfile(byte[] aid, byte[] aip, byte[] afl, byte[] pan, byte psn, byte[] par, byte[] track2, byte[] language,
                      Date effectiveDate, Date expirationDate, byte[] country, byte[] trid, byte[] last4digitsOfPan,
                      byte[] avn, byte cdolLength, boolean isRpSupported) {
        this.mAid = aid;
        this.mAip = aip;
        this.mAfl = afl;
        this.mPan = pan;
        this.mPanSequenceNumber = psn;
        this.mPar = par;
        this.mTrack2EquivalentData = track2;
        this.mLanguagePreference = language;
        this.mApplicationEffectiveDate = effectiveDate;
        this.mExpirationDate = expirationDate;
        this.mCardCountryCode = country;
        this.mTokenRequestorId = trid;
        this.mLast4DigitsOfPan = last4digitsOfPan;
        this.mApplicationVersionNumber = avn;
        this.mCdol1RelatedDataLength = cdolLength;
        this.misRemotePaymentSupported = isRpSupported;
    }

}