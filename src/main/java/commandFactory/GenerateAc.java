package commandFactory;

import mchipengine.utils.MchipUtils;

/**
 * Command template to generate an application cryptogram during an EMV transaction.
 */
public class GenerateAc {

    /**
     * The command APDU to be sent to the M/Chip Mobile application on the Secure Element.
     */
    private byte[] mCommand;

    /**
     * Create an array of byte[] commandApdu
     * append {0x80, 0xAE, 0x80, 0x00} to commandApdu
     * append cdol1RelatedData to commandApdu
     * mCommand = commandApdu
     *
     * @param cdol1RelatedData An array of bytes as per MCHIP.4
     */
    public GenerateAc(byte[] cdol1RelatedData) {
        MchipUtils.ByteArrayBuilder command = new MchipUtils.ByteArrayBuilder();
        command.add(new byte[]{(byte) 0x80, (byte) 0xAE, (byte) 0x80, (byte) 0x00});
        command.add(cdol1RelatedData);
        mCommand = command.build();
    }

    /**
     * Return the GENERATE AC command APDU to send to the M/Chip Mobile application on the SE.
     * Return mCommand.
     */
    public byte[] getCommandApdu() {
        return mCommand;
    }

}