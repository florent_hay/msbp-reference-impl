package commandFactory;

import mchipengine.utils.MchipUtils;

/**
 * Command template to retrieve a specific data from the card profile, identified by its tag.
 */
public class GetData {

    /**
     * The command APDU to be sent to the M/Chip Mobile application on the Secure Element.
     */
    private byte[] mCommand;

    /**
     * Create an array of byte[] commandApdu
     * append {0x80, 0xCA} to commandApdu
     * append tag to commandApdu
     * append 0x00 to commandApdu
     * mCommand = commandApdu
     *
     * @param tag
     */
    public GetData(byte[] tag) {
        MchipUtils.ByteArrayBuilder command = new MchipUtils.ByteArrayBuilder();
        command.add(new byte[]{(byte) 0x80, (byte) 0xCA});
        command.add(tag);
        command.add(new byte[]{0x00});
        mCommand = command.build();
    }

    /**
     * Return the GET DATA command APDU to send to the M/Chip Mobile application on the SE.
     * Return mCommand.
     */
    public byte[] getCommandApdu() {
        return mCommand;
    }

}