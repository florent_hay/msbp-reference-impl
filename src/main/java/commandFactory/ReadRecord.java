package commandFactory;

import mchipengine.utils.ByteUtils;
import mchipengine.utils.MchipUtils;

import java.util.Arrays;

/**
 * Command template to read records of the card profile.
 */
public class ReadRecord {

    /**
     * The command APDU to be sent to the M/Chip Mobile application on the Secure Element.
     */
    private byte[] mCommand;

    /**
     * Create an array of byte[] commandApdu
     * append {0x00, 0xB2} to commandApdu
     * append recordNumber to commandApdu
     * byte p2 = CONCATE(5 least significant bits of sfi, 100b)
     * append p2 to commandApdu
     * append 0x00 to commandApdu
     * mCommand = commandApdu
     *
     * @param recordNumber
     * @param sfi
     */
    public ReadRecord(byte recordNumber, byte sfi) {
        MchipUtils.ByteArrayBuilder command = new MchipUtils.ByteArrayBuilder();
        command.add(new byte[]{(byte) 0x00, (byte) 0xB2});
        command.add(recordNumber);
        byte p2 = ByteUtils.concatenateByteArrays(
                Arrays.asList(sfi, new byte[]{0x10, 0x00})
        );
        command.add(p2);
        command.add((byte) 0x00);
        mCommand = command.build();
    }

    /**
     * Return the READ RECORD command APDU to send to the M/Chip Mobile application on the SE.
     * Return mCommand.
     */
    public byte[] getCommandApdu() {
        return mCommand;
    }

}