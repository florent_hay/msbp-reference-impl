package commandFactory;

import mchipengine.utils.MchipUtils;

/**
 * Command template to initiate an EMV transaction.
 */
public class GetProcessingOptions {

    /**
     * The command APDU to be sent to the M/Chip Mobile application on the Secure Element.
     */
    private byte[] mCommand;

    /**
     * Create an array of byte[] commandApdu
     * append {0x80, 0xA8, 0x00, 0x00} to commandApdu
     * <p>
     * if pdolList is null {
     * append {0x02, 0x83, 0x00} to commandApdu
     * else {
     * <p>
     * append {0x0C, 0x83, 0x0A} to commandApdu
     * append {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00} to commandApdu
     * append terminalCountryCode to commandApdu
     * }
     * append 0x00 to commandApdu
     * mCommand = commandApdu
     *
     * @param pdolList The list of data elements requested to be provided by the M/Chip Mobile application in the GET PROCESSING OPTIONS command.
     */
    public GetProcessingOptions(byte[] pdolList, byte[] terminalCountryCode) {
        MchipUtils.ByteArrayBuilder command = new MchipUtils.ByteArrayBuilder();
        command.add(new byte[]{(byte) 0x80, (byte) 0xA8, (byte) 0x00, (byte) 0x00});
        if (pdolList == null) {
            command.add(new byte[]{(byte) 0x02, (byte) 0x83, (byte) 0x00});
        } else {
            command.add(new byte[]{(byte) 0x0C, (byte) 0x83, (byte) 0x0A});
            command.add(new byte[]{0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00});
            command.add(terminalCountryCode);
        }
        command.add(new byte[]{0x00});
        mCommand = command.build();
    }

    /**
     * Return the GET PROCESSING OPTIONS command APDU to send to the M/Chip Mobile application on the SE.
     * Return mCommand.
     */
    public byte[] getCommandApdu() {
        return mCommand;
    }

}