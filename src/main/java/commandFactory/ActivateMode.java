package commandFactory;

import models.McmMode;

/**
 * Command template to activate Remote Payment mode
 */
public class ActivateMode {

    /**
     * The command APDU to be sent to the M/Chip Mobile application on the Secure Element.
     */
    private byte[] mCommand;

    /**
     * if mcmMode == McmMode.REMOTE_PAYMENT {
     * mCommand: byte[] = {0x80, 0xE1, 0x00, 0x02}
     * }
     * else {
     * mCommand: byte[] = {0x80, 0xE1, 0x00, 0x03}
     * }
     *
     * @param mcmMode The mode to be selected on the M/Chip Mobile application, in order to process the transaction.
     */
    public ActivateMode(McmMode mcmMode) {
        switch (mcmMode) {
            case CONTACTLESS:
                mCommand = new byte[]{(byte) 0x80, (byte) 0xE1, (byte) 0x00, (byte) 0x01};
                break;
            case REMOTE_PAYMENT:
                mCommand = new byte[]{(byte) 0x80, (byte) 0xE1, (byte) 0x00, (byte) 0x02};
                break;
            case AUTHENTICATION:
                mCommand = new byte[]{(byte) 0x80, (byte) 0xE1, (byte) 0x00, (byte) 0x03};
                break;
            case MANAGEMENT:
            default:
                mCommand = new byte[]{(byte) 0x80, (byte) 0xE1, (byte) 0x00, (byte) 0x00};
                break;
        }
    }

    /**
     * Return the ACTIVATE MODE command APDU to send to the M/Chip Mobile application on the SE.
     * Return mCommand.
     */
    public byte[] getCommandApdu() {
        return mCommand;
    }

}