package models;

import org.junit.Assert;
import org.junit.Test;

public class DateTest {

    @Test
    public void compareDates() throws Exception {
        Date date1 = new Date(2020, 1, 1);
        Date date2 = new Date(2020, 2, 1);
        Date date3 = new Date(2020, 2, 1);

        Assert.assertEquals(date1.compareTo(date2), -1);
        Assert.assertEquals(date2.compareTo(date1), 1);

        Assert.assertEquals(date2.compareTo(date3), 0);
    }

    @Test
    public void validateFormat() throws Exception {
        Date date = new Date(2020, 2, 1);
        System.out.println(new String(date.getDateAsByteArray()));
    }
}
